<?php

use Illuminate\Http\Request;
use App\Http\Resources\SurvivorsResource;
use App\Http\Resources\UsersResource;
use App\Http\Resources\TribesResource;
use App\Http\Resources\AssignmentTribesResource;
use App\Http\Resources\DinosResource;
use App\Http\Resources\NotificationsResource;
use App\Survivors;
use App\User;
use App\Tribe;
use App\AssignmentTribes;
use App\Dinos;
use App\Notifications;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/users/{id}', function ($id) {
    return UsersResource::collection(User::where('id', $id)->get());
});

Route::get('/survivors/{id}', function ($id) {
    return SurvivorsResource::collection(Survivors::where('owner', $id)->get());
});

Route::get('/tribes/{id}', function ($id) {
    return TribesResource::collection(Tribe::where('owner', $id)->get());
});

Route::get('/tribes_fromid/{id}', function ($id) {
    return TribesResource::collection(Tribe::where('id', $id)->get());
});

Route::get('/assignment_tribes/{id}', function ($id) {
    return AssignmentTribesResource::collection(AssignmentTribes::where('sid', $id)->get());
});

Route::get('/notifications/{id}', function ($id) {
    return NotificationsResource::collection(Notifications::where('notification_owner', $id)->get());
});

Route::get('/tamed_dinos/{id}', function ($id) {
    return DinosResource::collection(Dinos::where('owner', $id)->get());
});

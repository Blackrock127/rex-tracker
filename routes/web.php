<?php

use App\Http\Controllers\AdminDashboard;
use App\Http\Controllers\Dashboard;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return redirect('/');
});

Route::get('/', 'WelcomeController@index');

Route::get('/logout', function() {
     Auth::logout();
     return redirect("/");
});

Auth::routes();

// Setup

Route::get('/setup', 'Setup@Setup');
Route::post('/setup/survivor', 'Setup@SetupSurvivor');
Route::get('/setup/new-tribe', 'Setup@Tribe');
Route::post('/setup/tribe', 'Setup@SetupTribe');

// Dashboard

Route::get('/dashboard', 'Dashboard\Dashboard@index');

Route::get('/tribe-logs', 'Dashboard\Dashboard@tribelogs');
Route::get('/tribe-invite/{id}', 'Dashboard\Dashboard@tribeInvite');
Route::get('/tribe-invite/{id}/accept', 'Dashboard\Dashboard@acceptInvite');
Route::get('/tribe-invite/{id}/reject', 'Dashboard\Dashboard@rejectInvite');
Route::get('/create-tribe-invite/{id}', 'Dashboard\Dashboard@createTribeInvite');

Route::get('/tamed-dinos', 'Dashboard\TamedDinos@getDinos');
Route::get('/tamed-dinos-new', 'Dashboard\TamedDinos@newDino');
Route::post('/tamed-dinos-create', 'Dashboard\TamedDinos@createDino');
Route::get('/tamed-dinos/{id}/edit', 'Dashboard\TamedDinos@editDino');
Route::post('/tamed-dinos/{id}/edit-save', 'Dashboard\TamedDinos@editsaveDino');
Route::get('/tamed-dinos/{id}/delete', 'Dashboard\TamedDinos@deleteDino');
Route::get('/tamed-dinos/{id}', 'Dashboard\TamedDinos@dinoProfile');

Route::get('/turret-timers', 'Dashboard\Timers@turret');
Route::get('/custom-timers', 'Dashboard\Timers@custom');

//Route::get('/taming-calculator', 'Dashboard\Dashboard@index');

//Route::get('/breeding-calculator', 'Dashboard\Dashboard@index');

//Route::get('/stats-calculator', 'Dashboard\Dashboard@index');

Route::get('/element-calculator', 'Dashboard\ElementCalculator@calculate');
Route::post('/element-calculator', 'Dashboard\ElementCalculator@calculate');

Route::get('/mortar-calculator', 'Dashboard\MortarCalculator@calculate');
Route::post('/mortar-calculator', 'Dashboard\MortarCalculator@calculate');

Route::get('/forge-calculator', 'Dashboard\ForgeCalculator@calculate');
Route::post('/forge-calculator', 'Dashboard\ForgeCalculator@calculate');

Route::get('/dino-network', 'Dashboard\DinoNetwork@index');
Route::post('/new-post', 'Dashboard\DinoNetwork@newPost');
Route::get('/search-users', 'Dashboard\DinoNetwork@searchUsers');
Route::post('/search-users', 'Dashboard\DinoNetwork@searchUsers');
Route::get('/profile/{id}', 'Dashboard\DinoNetwork@profile');

Route::get('/patch-notes', 'Dashboard\PatchNotes@index');

Route::get('/settings', 'Dashboard\Settings@settings');
Route::post('/settings/update', 'Dashboard\Settings@updateSettings');

Route::post('/notification-readed/{id}', 'Notification@index');

// Admin Dashboard

Route::get('/admin-dashboard', 'AdminDashboard\AdminDashboard@index');
Route::get('/admin-dashboard/users', 'AdminDashboard\AdminDashboard@manageUsers');
Route::get('/admin-dashboard/tribes', 'AdminDashboard\AdminDashboard@manageTribes');
Route::get('/admin-dashboard/management', 'AdminDashboard\AdminDashboard@index');

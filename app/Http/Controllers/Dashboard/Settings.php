<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AssignmentTribes;
use App\Survivors;
use App\Tribelogs;
use App\Tribe;
use App\Posts;
use App\DinoNetworkProfile;
use Auth;
use File;
use Image;
use App\User;

class Settings extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {

        $config = storage_path() . "/config.json";
        $json_config = json_decode(file_get_contents($config), true);
        $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

        $survivor = Survivors::where('owner', Auth::user()->id)->first();

        $survivors = Survivors::where('owner', Auth::user()->id)->get();



        return view('dashboard/settings')->with(['server_url' => $server_url]);
    }

    public function updateSettings(Request $request)
    {
        $user = User::where('id', Auth::user()->id)->first();
        if($request->hasFile('userPicture')) {
          $user_pic = $request->file('userPicture');
          $filename = time() . '.png';
          Image::make($user_pic)->save(public_path('/pics/src/users/') . $filename);
          $user->picture = $filename;
        }
        $user->save();

        $survivor = Survivors::where('owner', Auth::user()->id)->first();

        return redirect('dashboard');
    }
}

<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\AssignmentTribes;
use App\Fileentry;
use App\TribesDinoLimit;
use App\Survivors;
use App\Tribelogs;
use App\Tribe;
use App\Dinos;
use App\Http\Requests\CreateDinoRequest;
use Auth;
use File;
use Image;

class TamedDinos extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    //Get all tamed dinos from the tribe
    public function getDinos()
    {
      $config = storage_path() . "/config.json";
      $version = storage_path() . "/version.json";

      $json_config = json_decode(file_get_contents($config), true);
      $json_version = json_decode(file_get_contents($version), true);
      $theme = $json_config['config']['rex-tracker']['Theme'];
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      $survivor = Survivors::where('owner', Auth::user()->id)->first();

      $assignment_tribes_count = AssignmentTribes::where('sid', $survivor->id)->count();

      $has_tribe = false;

      if($assignment_tribes_count == 1)
      {

        $has_tribe = true;

        $assignment_tribes = AssignmentTribes::where('sid', $survivor->id)->first();

        $tribe = Tribe::where('id', $assignment_tribes->tid)->first();

        $dinos = Dinos::where('owner', $tribe->id)->get();

        //$dinos_pics = Dinos::where('owner', $tribe->id)->first();

        $dinos_count = Dinos::where('owner', $tribe->id)->count();

        $tribe_dino_limit = TribesDinoLimit::where('tid', $tribe->id)->first();

        //$dino_pic = Image::make($dinos_pics->dino_picture);

        //$response = Response::make($dino_pic->encode('jpeg'));

        //return $response;
        return view('dashboard/tamed-dinos')->with(['server_url' => $server_url, 'dinos-count' => $dinos_count, 'theme' => $theme, 'survivor' => $survivor, 'dinos' => $dinos, 'tribe' => $tribe, 'has_tribe' => $has_tribe, 'tribe_dino_limit' => $tribe_dino_limit, 'dinos_count' => $dinos_count]);

      } else {

        $has_tribe = false;

        return view('dashboard/tamed-dinos')->with(['server_url' => $server_url, 'theme' => $theme, 'survivor' => $survivor, 'has_tribe' => $has_tribe]);
      }

    }

    //Create a new dino
    public function newDino(Request $request)
    {
      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      $survivor = Survivors::where('owner', Auth::user()->id)->first();

      $assignment_tribes = AssignmentTribes::where('sid', $survivor->id)->first();

      $tribe = Tribe::where('id', $assignment_tribes->tid)->first();

      $tribe_dino_limit = TribesDinoLimit::where('tid', $tribe->id)->first();

      $dinos_count = Dinos::where('owner', $tribe->id)->count();

      if($dinos_count > $tribe_dino_limit->dino_limit - 1) {
        //Log
        $tribelogs = new Tribelogs;
        $tribelogs->owner = $tribe->id;
        $tribelogs->survivor_name = "";
        $tribelogs->log_message = "Dino Limit has been reached " . "(" . $dinos_count . "/" . $tribe_dino_limit->dino_limit . ")";
        $tribelogs->save();

        return redirect('/tamed-dinos');
      } else {

        return view('dashboard/new-tamed-dino')->with(['server_url' => $server_url]);
      }
    }

    //Create a new dino
    public function createDino(CreateDinoRequest $request)
    {

      $dino = new Dinos;

      $survivor = Survivors::where('owner', Auth::user()->id)->first();

      $assignment_tribes = AssignmentTribes::where('sid', $survivor->id)->first();

      $tribe = Tribe::where('id', $assignment_tribes->tid)->first();


      //Log Data
      $tribelogs = new Tribelogs;
      $tribelogs->owner = $tribe->id;
      $tribelogs->survivor_name = $survivor->name;
      $tribelogs->log_message = "Created a new dino " . "(" . $request['name'] . ")";
      $tribelogs->save();

      //Dino Picture
      if($request->hasFile('dinoPicture')) {
        $dino_pic = $request->file('dinoPicture');
        $filename = time() . '.png';
        Image::make($dino_pic)->save(public_path('/pics/src/tamed-dinos/') . $filename);
        $dino->dino_picture = $filename;
      }

      //Basic information
      $dino->owner = $tribe->id;
      $dino->name = $request['name'];
      $dino->type = $request['dino_type'];
      $dino->level = $request['level'];
      $dino->gender = "female";
      $dino->tamed_by = $tribe->name;
      $dino->created_by = $survivor->name;

      //Stats
      $dino->health = $request['health'];
      $dino->stamina = $request['stamina'];
      $dino->oxygen = $request['oxygen'];
      $dino->food = $request['food'];
      $dino->weight = $request['weight'];
      $dino->melee = $request['melee'];
      $dino->movement = $request['movement'];
      $dino->torpidity = $request['movement'];

      $dino->basehealth = $request['base_health'];
      $dino->basestamina = $request['base_stamina'];
      $dino->baseoxygen = $request['base_oxygen'];
      $dino->basefood = $request['base_food'];
      $dino->baseweight = $request['base_weight'];
      $dino->basemelee = $request['base_melee'];
      $dino->basemovement = $request['base_movement'];


      //Mutations and Imprints
      $dino->paternal_mut = $request['paternal_mutation'];
      $dino->maternal_mut = $request['maternal_mutation'];
      $dino->imprinting = $request['imprint'];


      $dino->save();


      return redirect('/tamed-dinos');
    }

    //Edit Dino
    public function editDino($id)
    {
      $dino_data = Dinos::where('id', $id)->first();

      return view('dashboard/edit-tamed-dino')->with(['dino_data' => $dino_data]);
    }

    public function editsaveDino(CreateDinoRequest $request, $id)
    {

      $dino = Dinos::where('id', $id)->first();

      $survivor = Survivors::where('owner', Auth::user()->id)->first();

      $assignment_tribes = AssignmentTribes::where('sid', $survivor->id)->first();

      $tribe = Tribe::where('id', $assignment_tribes->tid)->first();

      //Log Data
      $tribelogs = new Tribelogs;
      $tribelogs->owner = $tribe->id;
      $tribelogs->survivor_name = $survivor->name;
      $tribelogs->log_message = "Edited a dino " . "(" . $request['name'] . ")";
      $tribelogs->save();

      //Dino Picture
      if($request->hasFile('dinoPicture')) {

        if (File::exists(public_path('/pics/src/tamed-dinos/' . $dino->dino_picture))) {
            unlink(public_path('/pics/src/tamed-dinos/' . $dino->dino_picture));
        }
        $dino_pic = $request->file('dinoPicture');
        $filename = time() . '.png';
        Image::make($dino_pic)->save(public_path('/pics/src/tamed-dinos/') . $filename);
        $dino->dino_picture = $filename;
      }

      //Basic Information
      $dino->name = $request['name'];
      $dino->type = $request['dino_type'];
      $dino->level = $request['level'];
      $dino->gender = "female";

      //Stats
      $dino->health = $request['health'];
      $dino->stamina = $request['stamina'];
      $dino->oxygen = $request['oxygen'];
      $dino->food = $request['food'];
      $dino->weight = $request['weight'];
      $dino->melee = $request['melee'];
      $dino->movement = $request['movement'];
      $dino->torpidity = $request['movement'];

      $dino->basehealth = $request['base_health'];
      $dino->basestamina = $request['base_stamina'];
      $dino->baseoxygen = $request['base_oxygen'];
      $dino->basefood = $request['base_food'];
      $dino->baseweight = $request['base_weight'];
      $dino->basemelee = $request['base_melee'];
      $dino->basemovement = $request['base_movement'];

      //Mutation and Imprint
      $dino->paternal_mut = $request['paternal_mutation'];
      $dino->maternal_mut = $request['maternal_mutation'];
      $dino->imprinting = $request['imprint'];

      $dino->save();

      return redirect('/tamed-dinos');
    }

    //Delete Dino
    public function deleteDino($id)
    {

      $dino_data = Dinos::where('id', $id)->first();

      $survivor = Survivors::where('owner', Auth::user()->id)->first();

      $assignment_tribes = AssignmentTribes::where('sid', $survivor->id)->first();

      $tribe = Tribe::where('id', $assignment_tribes->tid)->first();

      $tribelogs = new Tribelogs;
      $tribelogs->owner = $tribe->id;
      $tribelogs->survivor_name = $survivor->name;
      $tribelogs->log_message = "Deleted a dino " . "(" . $dino_data->name . ")";
      $tribelogs->save();

      if (File::exists(public_path('/pics/src/tamed-dinos/' . $dino_data->dino_picture))) {
        unlink(public_path('/pics/src/tamed-dinos/' . $dino_data->dino_picture));
      }

      Dinos::where('id', $id)->delete();

      return redirect('/tamed-dinos');
    }

    //Dino Profile Page
    public function dinoProfile($id)
    {
      $survivor = Survivors::where('owner', Auth::user()->id)->first();

      $assignment_tribes_count = AssignmentTribes::where('sid', $survivor->id)->count();

      $has_tribe = false;

      if($assignment_tribes_count == 1)
      {

        $has_tribe = true;

        $assignment_tribes = AssignmentTribes::where('sid', $survivor->id)->first();

        $tribe = Tribe::where('id', $assignment_tribes->tid)->first();

        $dinos = Dinos::where('id', $id)->first();

        return view('dashboard/tamed-dinos-profile')->with(['survivor' => $survivor, 'dinos' => $dinos, 'tribe' => $tribe]);

      } else {

        $has_tribe = false;

        return redirect('dashboard/tamed-dinos');
      }

    }
}

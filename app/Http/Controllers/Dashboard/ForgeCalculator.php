<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Survivors;
use Auth;

class ForgeCalculator extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function calculate(Request $request)
    {

        $config = storage_path() . "/config.json";
        $json_config = json_decode(file_get_contents($config), true);
        $server_url = $json_config['config']['rex-tracker']['ServerUrl'];


        $forge_fuel = "wood"; //Default
        $calculate_time = 0;
        $calculate_resources = 0;

        if(!empty($request['resources_count']) && !empty($request['forge_count'])) {
          $forge_quantity = $request['forge_count']; //Default
          $forge_resource_to_refind = $request['fuel_type'];; //Default
          $resource_quantity = 	$request['resources_count']; //Default
        }  else {
          $forge_quantity = 0;
          $resource_quantity = 0;
          $resource_output = 0;
        }

        $thatch_time_p = 7.5;
        $wood_time_p = 30;
        $spark_time_p = 60;
        $angler_time_p = 240;

        $gas_time_i = 0;

        $r_metal = 20;

        $message = "";

        if ($resource_quantity % 2 == 0) {
        $message = "";
        //Formula
        if($forge_fuel == "thatch") {
          if(!empty($resource_quantity) && !empty($forge_quantity)) {
            $calculate_time = floor($resource_quantity / $forge_quantity);
            $calculate_time = ceil((($calculate_time / 2) * 20) / $thatch_time_p);
            $resource_output = $resource_quantity / 2;
          } else {
            //No input
            $calculate_time = 0;
            $calculate_resources = 0;
          }
        }
        if($forge_fuel == "wood") {
          $image = "";
          if(!empty($resource_quantity) && !empty($forge_quantity)) {
            $calculate_time = floor($resource_quantity / $forge_quantity);
            $calculate_time = ceil((($calculate_time / 2) * 20) / $wood_time_p);
            $resource_output = $resource_quantity / 2;
          } else {
            //No input
            $calculate_time = 0;
            $calculate_resources = 0;
          }
        }
        if($forge_fuel == "spark") {
          if(!empty($resource_quantity) && !empty($forge_quantity)) {
            $calculate_time = floor($resource_quantity / $forge_quantity);
            $calculate_time = ceil((($calculate_time / 2) * 20) / $spark_time_p);
            $resource_output = $resource_quantity / 2;
          } else {
            //No input
            $calculate_time = 0;
            $calculate_resources = 0;
          }
        }
        if($forge_fuel == "angler") {
          if(!empty($resource_quantity) && !empty($forge_quantity)) {
            $calculate_time = floor($resource_quantity / $forge_quantity);
            $calculate_time = ceil((($calculate_time / 2) * 20) / $angler_time_p);
            $resource_output = $resource_quantity / 2;
          } else {
            //No input
            $calculate_time = 0;
            $calculate_resources = 0;
          }
        }
        if($forge_fuel == "gas") {
          if(!empty($resource_quantity) && !empty($forge_quantity)) {
            $calculate_time = floor($resource_quantity / $forge_quantity);
            $calculate_time = ceil((($calculate_time / 2) * 20) / $gas_time_i);
            $resource_output = $resource_quantity / 2;
          } else {
            //No input
            $calculate_time = 0;
            $calculate_resources = 0;
          }
        }
      } else {
        $resource_output = 0;
        $calculate_time = 0;
        $calculate_resources = 0;
        if($resource_quantity == 1) {
          $message = "Unable to refine " . $resource_quantity . " Metal, Try " . ($resource_quantity + 1) ." instead!";
        } else {
          $message = "Unable to refine " . $resource_quantity . " Metal, Try " . ($resource_quantity - 1) ." instead!";
        }
      }
      return view('dashboard/forge-calculator')->with(['server_url' => $server_url, 'message' => $message, 'resource_output' => $resource_output, 'calculate_time' => $calculate_time, 'calculate_resources' => $calculate_resources, 'forge_quantity' => $forge_quantity, 'resource_quantity' => $resource_quantity]);
  }
}

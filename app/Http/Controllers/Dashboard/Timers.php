<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\TimersTable;

class Timers extends Controller
{
  /* public function index()
  {
      $timers = TimersTable::where('owner', Auth::user()->id)->get();

      return view('dashboard/custom-timers')->with(['timers' => $timers]);
  } */

  public function turret()
  {
      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      return view('dashboard/turret-not-available')->with(['server_url' => $server_url]);
  }

  public function custom()
  {
      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      return view('dashboard/custom-not-available')->with(['server_url' => $server_url]);
  }
}

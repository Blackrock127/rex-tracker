<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Survivors;
use Auth;

class ElementCalculator extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard/element-calculator');
    }

    public function calculate(Request $request)
    {

        $config = storage_path() . "/config.json";
        $json_config = json_decode(file_get_contents($config), true);
        $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

        $time_required = 0;
        $resource_output = 0;
        $craft_per_ammount = 1;

        $green_gem = 20;
        $blue_gem = 15;
        $red_gem = 10;
        $charge_battery = 8;
        $gas = 50;
        $element_ore = 20;

        //calculate
        $craft_ammount = 0;
        $craft_ammount = $request['element_count'];

        $green_gem = 20 * $craft_ammount;
        $blue_gem = 15 * $craft_ammount;
        $red_gem = 10 * $craft_ammount;
        $charge_battery = 8 * $craft_ammount;
        $gas = 50 * $craft_ammount;
        $element_ore = 20 * $craft_ammount;

        //calculate time in secs

        $time_required = 2580 * $craft_ammount / 3 / 60;

        //calculate resource output

        $resource_output = $craft_ammount;

        if(empty($resource_output)) {
          $resource_output = 0;
        }

        $message = "";
        if($craft_ammount > 100000) {
          $message = "Too mutch element to craft! Try less.";
          $green_gem = 0;
          $blue_gem = 0;
          $red_gem = 0;
          $charge_battery = 0;
          $gas = 0;
          $element_ore = 0;
          $resource_output = 0;
          $time_required = 0;
        }

        return view('dashboard/element-calculator')->with(['server_url' => $server_url, 'message' => $message, 'time_required' => $time_required, 'resource_output' => $resource_output, 'green_gem' => $green_gem, 'blue_gem' => $blue_gem, 'red_gem' => $red_gem, 'charge_battery' => $charge_battery, 'gas' => $gas, 'element_ore' => $element_ore]);
    }
}

<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AssignmentTribes;
use App\Survivors;
use App\TribeInvitations;
use App\DinoNetworkProfile;
use App\Notifications;
use App\Tribelogs;
use App\Tribe;
use Auth;

class Dashboard extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = storage_path() . "/config.json";
        $version = storage_path() . "/version.json";

        $json_config = json_decode(file_get_contents($config), true);
        $json_version = json_decode(file_get_contents($version), true);

        $theme = $json_config['config']['rex-tracker']['Theme'];
        $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

        if(Auth::user()->first_login == "TRUE") {
            return redirect('/setup');
        } else {

            $theme = $json_config['config']['rex-tracker']['Theme'];

            $survivor = Survivors::where('owner', Auth::user()->id)->first();

            $survivors = Survivors::where('owner', Auth::user()->id)->get();

            $survivor_id = $survivor->id;

            $assignment_tribes_count = AssignmentTribes::where('sid', $survivor_id)->count();

            $has_tribe = false;

            $notifications = Notifications::where('notification_owner', Auth::user()->id)->get();

            $notifications_count = Notifications::where('notification_owner', Auth::user()->id)->count();

            if($assignment_tribes_count == 1)
            {
              $theme = $json_config['config']['rex-tracker']['Theme'];

              $has_tribe = true;

              $assignment_tribes = AssignmentTribes::where('sid', $survivor_id)->first();

              $tribe = Tribe::where('id', $assignment_tribes->tid)->first();

              $tribe_members = AssignmentTribes::where('tid', $tribe->id)->orderBy('role')->get();

              $survivor_tribe_manager = AssignmentTribes::where('sid', $survivor->id)->first();

              return view('dashboard/dashboard')->with(['server_url' => $server_url, 'theme' => $theme, 'notifications_count' => $notifications_count, 'notifications' => $notifications, 'survivor_tribe_manager' => $survivor_tribe_manager, 'tribe_members' => $tribe_members, 'survivors' => $survivors, 'survivor' => $survivor, 'has_tribe' => $has_tribe, 'tribe' => $tribe]);

            } else {

              $theme = $json_config['config']['rex-tracker']['Theme'];

              $has_tribe = false;

              return view('dashboard/dashboard')->with(['server_url' => $server_url, 'theme' => $theme, 'notifications_count' => $notifications_count, 'notifications' => $notifications,'survivors' => $survivors, 'survivor' => $survivor, 'has_tribe' => $has_tribe]);
            }

        }
    }

    public function createTribeInvite($id) {
      return redirect('/dashboard');
    }

    /* public function createTribeInvite($id) {
      $dino_network_profile = DinoNetworkProfile::where('id', $id)->first();
      $survivor = Survivors::where('id', $dino_network_profile->sid)->first();
      $survivor_owner = $survivor->owner;

      $my_survivor = Survivors::where('owner', Auth::user()->id)->first();

      $assignment_tribes = AssignmentTribes::where('sid', $my_survivor->id)->first();
      $tribe = Tribe::where('id', $assignment_tribes->tid)->first();

      //Log Data
      $tribelogs = new Tribelogs;
      $tribelogs->owner = $tribe->id;
      $tribelogs->survivor_name = $survivor->name;
      $tribelogs->log_message = "has been invited to this tribe";
      $tribelogs->save();

      $tribe_invitation = new TribeInvitations;
      $tribe_invitation->invitation_sent_by = $tribe->id;
      $tribe_invitation->invitation_owner = $survivor_owner;
      $tribe_invitation->tribe_name = $tribe->name;
      $tribe_invitation->save();

      $tribe_invitation_get = TribeInvitations::where('invitation_owner', $survivor_owner)->first();
      $tribe_invitation_id = $tribe_invitation_get->id;

      $notification = new Notifications;

      $notification->notification_owner = $survivor_owner;
      $notification->notification_sent_by = Auth::user()->name;
      $notification->notification_type = "TribeNotifiaction";
      $notification->notification_content = "You have been invited to " . $tribe->name;
      $notification->custom_link = "/tribe-invite/" . $tribe_invitation_id;

      $notification->save();

      return redirect('/dashboard');
    } */

    /* public function acceptInvite($id) {
      $tribe_invitation_get = TribeInvitations::where('id', $id)->first();
      $tribe = $tribe_invitation_get->invitation_sent_by;

      $my_survivor = Survivors::where('owner', Auth::user()->id)->first();
      $assignment_tribes_count = AssignmentTribes::where('sid', $my_survivor->id)->count();

      if($assignment_tribes_count == 1)
      {
        $tribe_ass = AssignmentTribes::where('sid', $my_survivor->id)->first();

        //Log Data
        $tribelogs = new Tribelogs;
        $tribelogs->owner = $tribe_ass->tid;
        $tribelogs->survivor_name = $my_survivor->name;
        $tribelogs->log_message = "was removed from the tribe.";
        $tribelogs->save();

        AssignmentTribes::where('sid', $my_survivor->id)->delete();
      }

      $new_assignment_tribes = new AssignmentTribes;

      $new_assignment_tribes->name = $my_survivor->name;
      $new_assignment_tribes->sid = $my_survivor->id;
      $new_assignment_tribes->tid = $tribe;
      $new_assignment_tribes->role = "Member";
      $new_assignment_tribes->save();

      $tribelogs = new Tribelogs;
      $tribelogs->owner = $tribe;
      $tribelogs->survivor_name = $my_survivor->name;
      $tribelogs->log_message = "was added to the tribe.";
      $tribelogs->save();

      $notification = new Notifications;

      $notification::where('custom_link','/tribe-invite' . '/' . $tribe_invitation_get->id)->delete();

      return redirect('/dashboard');
    } */

    /* public function rejectInvite($id) {

      TribeInvitations::where('id', $id)->delete();

      return redirect('/dashboard');
    } */

    /* public function tribeInvite($id) {

      $survivor = Survivors::where('owner', Auth::user()->id)->first();

      $tribe_invitation_get = TribeInvitations::where('id', $id)->first();

      return view('dashboard/tribe-invite')->with(['tribe_invitation_get' => $tribe_invitation_get]);
    } */

    public function tribelogs() {
      $survivor = Survivors::where('owner', Auth::user()->id)->first();
      $assignment_tribes = AssignmentTribes::where('sid', $survivor->id)->first();
      $tribe = Tribe::where('id', $assignment_tribes->tid)->first();

      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      $tribelogs = Tribelogs::where('owner', $tribe->id)->orderBy('created_at','desc')->get();

      return view('dashboard/tribe-logs')->with(['tribelogs' => $tribelogs, 'server_url' => $server_url]);
    }
}

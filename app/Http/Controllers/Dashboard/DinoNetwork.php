<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AssignmentTribes;
use App\Survivors;
use App\Tribelogs;
use App\Tribe;
use App\Posts;
use App\DinoNetworkProfile;
use Auth;

class DinoNetwork extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $config = storage_path() . "/config.json";
        $json_config = json_decode(file_get_contents($config), true);
        $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

        $survivor = Survivors::where('owner', Auth::user()->id)->first();

        $survivors = Survivors::where('owner', Auth::user()->id)->get();

        $posts = Posts::orderBy('created_at','desc')->paginate(15);

        $dino_network_profile = DinoNetworkProfile::where('sid', $survivor->id)->first();

        if($dino_network_profile == Auth::user()->picture) {

        } else {
          $profile = DinoNetworkProfile::where('sid', $survivor->id)->first();
          $profile->picture = Auth::user()->picture;
          $profile->save();
        }

        return view('dashboard/dino-network')->with(['server_url' => $server_url, 'dino_network_profile' => $dino_network_profile, 'posts' => $posts, 'survivors' => $survivors, 'survivor' => $survivor]);
    }

    public function profile($id)
    {
        $config = storage_path() . "/config.json";
        $json_config = json_decode(file_get_contents($config), true);
        $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

        $survivor = Survivors::where('owner', Auth::user()->id)->first();

        $survivors = Survivors::where('owner', Auth::user()->id)->get();

        $dino_network_profile = DinoNetworkProfile::where('id', $id)->first();

        $profile = DinoNetworkProfile::where('sid', $survivor->id)->first();

        $posts = Posts::where('created_by', $id)->orderBy('created_at','desc')->paginate(15);

        return view('dashboard/profile')->with(['server_url' => $server_url, 'posts' => $posts, 'profile' => $profile, 'dino_network_profile' => $dino_network_profile, 'survivors' => $survivors, 'survivor' => $survivor]);
    }

    public function newPost(Request $request)
    {
      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      $survivor = Survivors::where('owner', Auth::user()->id)->first();

      $survivors = Survivors::where('owner', Auth::user()->id)->get();

      $profile = DinoNetworkProfile::where('sid', $survivor->id)->first();

      $dino_network_profile = DinoNetworkProfile::where('id', $profile->id)->first();

      $post = new Posts;

      $post->created_by = $dino_network_profile->id;
      $post->creator_name = $dino_network_profile->name;
      $post->creator_picture = Auth::user()->picture;
      $post->post_content = $request['content'];
      $post->post_image = null;

      $post->save();

      return redirect('/dino-network');
    }

    public function searchUsers(Request $request)
    {
      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      $survivor = Survivors::where('owner', Auth::user()->id)->first();

      $survivors = Survivors::where('owner', Auth::user()->id)->get();

      $profiles = DinoNetworkProfile::paginate(35);

      return view('dashboard/search-users')->with(['server_url' => $server_url, 'profiles' => $profiles, 'survivors' => $survivors, 'survivor' => $survivor]);
    }
}

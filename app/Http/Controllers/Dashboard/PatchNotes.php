<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Survivors;
use Auth;

class PatchNotes extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      return view('dashboard/patch-notes')->with(['server_url' => $server_url]);
    }
}

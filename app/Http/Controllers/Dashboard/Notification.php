<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AssignmentTribes;
use App\Survivors;
use App\Tribe;
use App\User;
use App\TribesDinoLimit;
use DB;
use App\Notifications;
use Auth;

class Notification extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index($id, Request $request)
    {
      $notification = Notifications::where('id', $id)->first();
      $notification->isSeen = $request['isSeen'];
      $notification->save();
    }

}

<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Survivors;
use Auth;

class MortarCalculator extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard/mortar-calculator');
    }

    public function calculate(Request $request)
    {

        $config = storage_path() . "/config.json";
        $json_config = json_decode(file_get_contents($config), true);
        $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

        $time_required = 0;
        $resource_output = 0;
        $craft_per_ammount = 1;
        $multiplyer = 1;

        $crafting_type = "mp";
        $crafting_type = $request['crafting_type'];

        if($crafting_type == "mp") {
          $multiplyer = 1;
          $xp = 2.0;
        } else {
          $multiplyer = 6;
          $xp = 8.0;
        }

        //calculate
        $craft_ammount = 0;
        $craft_ammount = $request['narco_amount'];

        if($crafting_type == "mp") {
          $narcoberry = 5 * $craft_ammount;
          $spoiled_meat = 1 * $craft_ammount;
          $mp_selected = "selected";
          $cp_selected = "";
        } else {
          $narcoberry = 20 * $craft_ammount;
          $spoiled_meat = 4 * $craft_ammount;
          $mp_selected = "";
          $cp_selected = "selected";
        }

        //calculate time in secs

        $time_required = 5 * $craft_ammount;

        //calculate resource output

        $resource_output = $craft_ammount * 6;

        if(empty($resource_output)) {
          $resource_output = 0;
        }

        $message = "";
        if($craft_ammount > 100000) {
          $message = "Too mutch narcotics to craft! Try less.";
          $resource_output = 0;
          $narcoberry = 0;
          $spoiled_meat = 0;
          $xp = 0;
          $time_required = 0;
        }

        //calculate_XP
        $xp = $xp * $craft_ammount;


        return view('dashboard/mortar-calculator')->with(['server_url' => $server_url, 'mp_selected' => $mp_selected, 'cp_selected' => $cp_selected, 'message' => $message, 'time_required' => $time_required, 'resource_output' => $resource_output, 'narcoberry' => $narcoberry, 'spoiled_meat' => $spoiled_meat, 'xp' => $xp]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AssignmentTribes;
use App\Survivors;
use App\Tribe;
use App\User;
use App\TribesDinoLimit;
use DB;
use Auth;

class WelcomeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
      $config = storage_path() . "/config.json";
      $version = storage_path() . "/version.json";

      $json_config = json_decode(file_get_contents($config), true);
      $json_version = json_decode(file_get_contents($version), true);

      $admin_created = $json_config['config']['rex-tracker']['ServerAdminCreated'];
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];


      if($admin_created == "false") {
        return view('/errors/no-admin');
      } else {
        return view('welcome')->with(['server_url' => $server_url]);
      }
    }

}

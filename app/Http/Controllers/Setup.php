<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AssignmentTribes;
use App\Survivors;
use App\Tribe;
use App\User;
use App\DinoNetworkProfile;
use App\TribesDinoLimit;
use App\Http\Requests\Survivor;
use DB;
use Auth;

class Setup extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function Setup()
    {
      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      $first_login = Auth::user()->first_login;

      if($first_login == "TRUE") {
        return view('new-survivor')->with(['server_url' => $server_url]);
      } else {
        return redirect('dashboard');
      }
    }

    public function Tribe()
    {
      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      $first_login = Auth::user()->first_login;

      if($first_login == "TRUE") {
        return view('new-tribe')->with(['server_url' => $server_url]);
      } else {
        return redirect('dashboard');
      }
    }

    public function SetupSurvivor(Survivor $request)
    {
      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      $survivors = new Survivors;
      $survivors->name = $request['survivor_name'];
      //PRIVATE FUNCTION
      if($request['remember'] == "on") {
        $survivors->server = "Private Server";
      } else {
        $server_platform = $request['server_platform'] . $request['delicated_server_platform'];

        $survivors->server = $request = $server_platform . " - " . $request['server_type'] . " " .  $request['server_number'] . " " . $request['server_map'];
      }
      //PRIVATE FUNCTION

       $survivors->owner = Auth::user()->id;

       $survivors->save();

       //Create Dino Network profile

       // Essentially you're creating a survivor before this code and then saving it

       // Now you're re-fetching it from the database for no reason. But for some reason this
       // doesn't seem to be fetching anything back since the name field is entered correctly.
       $survivor = Survivors::where('owner', Auth::user()->id)->first();

       // So to correct the error, just get the id from the survivor you created before (switch $survivor-> to $survivors->)

       $dino_network_profile = new DinoNetworkProfile;
       $dino_network_profile->sid = $survivor->id;
       $dino_network_profile->name = $survivor->name;
       $dino_network_profile->tag = "@". str_replace(' ', '', $survivor->name) . rand(1000, 9999);
       $dino_network_profile->save();

       return redirect('/setup/new-tribe');

    }

    public function SetupTribe(Request $request)
    {
      $config = storage_path() . "/config.json";
      $json_config = json_decode(file_get_contents($config), true);
      $server_url = $json_config['config']['rex-tracker']['ServerUrl'];

      if($request['remember'] == "on") {

        User::where('id', Auth::user()->id)->update(['first_login' => "FALSE"]);
        return redirect('dashboard');

      } else {

        $survivor = Survivors::where('owner', Auth::user()->id)->first();

        $tribe = new Tribe;
        $tribe->name = $request['tribe_name'];
        $tribe->owner = Auth::user()->id;
        $tribe->save();

        //Logs

        $tribe = Tribe::where('owner', Auth::user()->id)->first();

        $assignment_tribes = new AssignmentTribes;
        $assignment_tribes->name = $survivor->name;
        $assignment_tribes->sid = $survivor->id;
        $assignment_tribes->tid = $tribe->id;
        $assignment_tribes->role = "Owner";
        $assignment_tribes->save();

        $tribe_dino_limit = new TribesDinoLimit;

        $tribe_dino_limit->tid = $tribe->id;
        $tribe_dino_limit->dino_limit = 25;
        $tribe_dino_limit->save();

        User::where('id', Auth::user()->id)->update(['first_login' => "FALSE"]);
        return redirect('dashboard');

      }
    }

}

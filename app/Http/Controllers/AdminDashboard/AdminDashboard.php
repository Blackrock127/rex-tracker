<?php

namespace App\Http\Controllers\AdminDashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AssignmentTribes;
use App\Survivors;
use App\Tribelogs;
use App\Tribe;
use Auth;

class AdminDashboard extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
              $survivor = Survivors::where('owner', Auth::user()->id)->first();

              $has_tribe = true;

              $assignment_tribes = AssignmentTribes::where('sid', $survivor->id)->first();

              $tribe = Tribe::where('id', $assignment_tribes->tid)->first();

              $tribe_members = AssignmentTribes::where('tid', $tribe->id)->orderBy('role')->get();

              $survivor_tribe_manager = AssignmentTribes::where('tid', $tribe->id)->first();

              return view('admin-dashboard/admin-dashboard')->with(['survivor_tribe_manager' => $survivor_tribe_manager, 'tribe_members' => $tribe_members, 'survivor' => $survivor, 'has_tribe' => $has_tribe, 'tribe' => $tribe]);
    }

    public function manageUsers()
    {
              return view('admin-dashboard/manage-users');
    }

    public function manageTribes()
    {
              return view('admin-dashboard/manage-tribes');
    }

}

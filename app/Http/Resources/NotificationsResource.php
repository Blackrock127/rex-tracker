<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class NotificationsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
          'id' => $this->id,
          'notification_owner' => $this->notification_owner,
          'notification_sent_by' => $this->notification_sent_by,
          'notification_content' => $this->notification_content,
          'notification_type' => $this->notification_type,
          'custom_link' => $this->custom_link,
          'isSeen' => $this->isSeen,
      ];
    }
}

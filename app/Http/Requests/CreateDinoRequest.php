<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateDinoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'dino_type' => 'required',
            'level' => 'required',
            'dinoPicture' => 'image | max:5000',
            'health' => 'required',
            'stamina' => 'required',
            'oxygen' =>'required',
            'food' => 'required',
            'weight' => 'required',
            'melee' =>'required',
            'movement' => 'required',
            'torpidity' => 'required',
        ];
    }
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable {
    use UuidModelTrait, Notifiable, SoftDeletes, CanResetPassword;

    protected $fillable = ['id','name', 'email', 'password', 'role', 'photo', 'forcechpwd'];
    protected $hidden = ['password', 'remember_token', 'email_verified', 'email_vercode', 'admin_verified'];
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];
    public $incrementing = false;
    protected $table = "users";
}

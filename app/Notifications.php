<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Notifications extends Model {
    use UuidModelTrait, SoftDeletes;

    protected $fillable = ['notification_owner','notification_sent_by','notification_type','notification_content','custom_link'];
    protected $hidden = [];
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];
    public $incrementing = false;
    protected $table = "notifications";
}

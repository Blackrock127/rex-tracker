<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class DinoNetworkProfile extends Model {
    use UuidModelTrait, SoftDeletes;

    protected $fillable = ['id','sid','name','tag'];
    protected $hidden = [];
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];
    public $incrementing = false;
    protected $table = "dino_network_profile";
}

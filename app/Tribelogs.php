<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Tribelogs extends Model {
    use UuidModelTrait, SoftDeletes;

    protected $fillable = ['survivor_name','log_message'];
    protected $hidden = [];
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];
    public $incrementing = false;
    protected $table = "tribelogs";
}

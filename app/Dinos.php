<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Dinos extends Model {
    use UuidModelTrait, SoftDeletes;

    protected $fillable = ['dino_picture','tamed_by','created_by','owner','gender','name','type','level','health','stamina','oxygen','food','water','weight','melee','movement','torpidity','baselevel','basehealth','basestamina','baseoxygen','basefood','basewater','baseweight','basemelee','basemovement','imprinting','paternal_mut','maternal_mut'];
    protected $hidden = [];
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];
    public $incrementing = false;
    protected $table = "dinos";
}

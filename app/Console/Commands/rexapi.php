<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Tribe;
use App\AssignmentTribes;
use App\Survivors;
use App\TamedDinos;

class rexapi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rexapi {rexcommand?} {options?} {options1?} {options2?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Controls the Rex API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      if($this->argument('rexcommand') == "") {
        $this->getOutput()->setDecorated(true);
        $this->line("===========================================");
        $this->line("RexAPI v5.14");
        $this->line("Here is a list of all Rex Tracker Commands!");
        $this->line(" ");
        $this->line("[help] Gets a list of all commands available.");
        $this->line("[version] Get the current RexAPI version.");
        $this->line("[test] Test the Rex API Data Reader and Writer.");
        $this->line("===========================================");
      }

      if($this->argument('rexcommand') == "help") {
        $this->getOutput()->setDecorated(true);
        $this->line("===========================================");
        $this->line("RexAPI v5.14");
        $this->line("Here is a list of all Rex Tracker Commands!");
        $this->line(" ");
        $this->line("[help] Gets a list of all commands available.");
        $this->line("[version] Get the current RexAPI version.");
        $this->line("[test] Test the Rex API Data Reader and Writer.");
        $this->line("===========================================");
      }

      if($this->argument('rexcommand') == "help") {
        $this->getOutput()->setDecorated(true);
        $this->line("RexAPI v5.14");
      }

      if($this->argument('rexcommand') == "test-version") {
        $path = storage_path() . "/version.json";
        $json = json_decode(file_get_contents($path), true);
        $this->line("Reading version.json...");
        sleep(3);
        $this->line("[INFO] Rex Tracker Version : " . $json['build-info']['rex-version']);
        $this->line("[INFO] RexAPI Version : " . $json['build-info']['rexapi-version']);
        $this->line("[INFO] Build ReleaseDate : " . $json['build-info']['ReleaseDate']);
      }

      $admin_id = "09351c28-fbd8-4ec3-8639-d9b85a1883a5";
      $tribe_id = "";
      $survivor_id = "";

      $error = 0;
      $error_array = [];

      if($this->argument('rexcommand') == "test") {
        $this->getOutput()->setDecorated(true);

        $this->line("Starting RexAPI Write & Read Test!");
        sleep(0.5);
        $this->line("Getting json data...");
        sleep(0.5);
        $this->line("Reading json data...");
        sleep(0.5);
        $this->line("Getting User Data...");
        $this->line(file_get_contents("http://rex-tracker.wcksoft.com/api/users/" . $admin_id));
        sleep(0.5);
        $this->info("Getting User Data... DONE!");
        $this->line("Getting Tribe Data...");
        $this->line(file_get_contents("http://rex-tracker.wcksoft.com/api/tribes/" . $admin_id));
        $tmp_tribe_id = file_get_contents("http://rex-tracker.wcksoft.com/api/tribes/" . $admin_id);
        $data_tribe_id = json_decode($tmp_tribe_id, true);
        $tribe_id = $data_tribe_id['data'][0]['id'];
        $this->line("Saved Tmp Data : " . $tribe_id . "!");
        sleep(0.5);
        $this->info("Getting Tribe Data... DONE!");
        sleep(0.5);
        $this->line("Getting Survivor Data...");
        $this->line(file_get_contents("http://rex-tracker.wcksoft.com/api/survivors/" . $admin_id));
        $this->line("Saving Tmp Data....");
        $tmp_survivor_id = file_get_contents("http://rex-tracker.wcksoft.com/api/survivors/" . $admin_id);
        $survivors = json_decode($tmp_survivor_id, true);
        $survivor_id = $survivors['data'][0]['id'];
        $this->info("Saved Tmp Data : " . $survivor_id . "!");
        sleep(0.5);
        $this->info("Getting Survivor Data... DONE!");
        sleep(0.5);
        $this->line("Getting Assignment Tribe Data...");
        $this->line(file_get_contents("http://rex-tracker.wcksoft.com/api/assignment_tribes/" . $survivor_id));
        sleep(0.5);
        $this->info("Getting Assignement Tribe Data... DONE!");
        sleep(0.5);
        $this->line("Verifying json data...");
        $config = storage_path() . "/config.json";
        $version = storage_path() . "/version.json";

        $json_config = json_decode(file_get_contents($config), true);
        $json_version = json_decode(file_get_contents($version), true);

        sleep(0.5);
        $this->line("Testing User Data...");
        $db_user = User::where('id', $admin_id)->first();
        $tmp_json_user = file_get_contents("http://rex-tracker.wcksoft.com/api/survivors/" . $admin_id);
        $json_user = json_decode($tmp_json_user , true);
        $get_json_user = $json_user['data'][0];


        sleep(0.5);
        $this->line("User Data : JSON:'" . $get_json_user['name'] . "' => DB:'" . $db_user->name . "' ");
        sleep(0.5);
        if(!$get_json_user['name'] == $db_user->name) {
        $error_array[] = "User Data : JSON:'" . $get_json_user['name'] . "' => DB:'" . $db_user->name . "' ";
        $this->error("Testing User Data... FAILED!");
        $error++;
        } else {
          $this->info("Testing User Data... DONE!");
        }
        sleep(0.5);
        $this->line("Testing Tribe Data...");
        $db_tribe = Tribe::where('owner', $admin_id)->first();
        $tmp_json_tribe = file_get_contents("http://rex-tracker.wcksoft.com/api/tribes/" . $admin_id);
        $json_tribe = json_decode($tmp_json_tribe , true);
        $get_json_tribe = $json_tribe['data'][0];

        sleep(0.5);
        $this->line("Tribe Data : JSON:'" . $get_json_tribe['name'] . "' => DB:'" . $db_tribe->name . "' ");
        sleep(0.5);
        if($get_json_tribe['name'] == $db_tribe->name) {
          $this->info("Testing Tribe Data... DONE!");
        } else {
          $this->error("Testing Tribe Data... FAILED!");
          $error_array[] = "Tribe Data : JSON:'" . $get_json_tribe['name'] . "' => DB:'" . $db_tribe->name . "' ";
          $error++;
        }
        sleep(0.5);
        $this->line("Testing Survivor Data...");

        $db_survivor = Survivors::where('owner', $admin_id)->first();

        $tmp_json_survivors = file_get_contents("http://rex-tracker.wcksoft.com/api/survivors/" . $admin_id);
        $json_survivors = json_decode($tmp_json_survivors , true);
        $get_json_survivors = $json_survivors['data'][0];


        sleep(0.5);
        $this->line("Tribe Data : JSON:'" . $get_json_survivors['name'] . "' => DB:'" . $db_survivor->name . "' ");
        sleep(0.5);
        if($get_json_survivors['name'] == $db_survivor->name) {
          $this->info("Testing Survivor Data... DONE!");
        } else {
          $error_array[] = "Tribe Data : JSON:'" . $get_json_survivors['name'] . "' => DB:'" . $db_survivor->name . "' ";
          $this->error("Testing Survivor Data... FAILED!");
          $error++;
        }
        sleep(0.5);
        $this->line("Testing Assignment Tribe Data...");
        $this->line(file_get_contents("http://rex-tracker.wcksoft.com/api/assignment_tribes/" . $survivor_id));
        $tmp_tribe_role = file_get_contents("http://rex-tracker.wcksoft.com/api/assignment_tribes/" . $survivor_id);
        $role = json_decode($tmp_tribe_role, true);

        $this->line(file_get_contents("http://rex-tracker.wcksoft.com/api/tribes_fromid/" . $role['data'][0]['tid']));
        $tmp_tribe_assigment = file_get_contents("http://rex-tracker.wcksoft.com/api/tribes_fromid/" . $role['data'][0]['tid']);
        $tribe = json_decode($tmp_tribe_assigment, true);

        $this->line("Tribe Name : " . $tribe['data'][0]['name']);
        $this->line("Role : " . $role['data'][0]['role']);
        sleep(0.5);
        $this->info("Testing Assignment Tribe Data... DONE!");
        sleep(0.5);
        $this->line("Testing Tamed Dinos Data...");
        $this->line(file_get_contents("http://rex-tracker.wcksoft.com/api/tamed_dinos/" . $tribe_id));
        $tmp_dinos = file_get_contents("http://rex-tracker.wcksoft.com/api/tamed_dinos/" . $tribe_id);
        $dinos = json_decode($tmp_dinos, true);
        foreach($dinos['data'] as $obj[0]){
          $this->line("Tamed Dinos Data : JSON:'" . $obj[0]["id"] . ";". $obj[0]["name"] . ";". $obj[0]["type"] . ";". $obj[0]["level"]  . "' => DB:'null' ");
        }
        sleep(0.5);
        foreach($dinos['data'] as $obj[0]){
          $error_array[] = "Tamed Dinos Data : JSON:'" . $obj[0]["id"] . ";". $obj[0]["name"] . ";". $obj[0]["type"] . ";". $obj[0]["level"]  . "' => DB:'null' ";
        }
        $this->error("Testing Tamed Dinos Data... FAILED!");
        $error++;
        sleep(0.5);
        $this->info("Verifying json data ... DONE!");
        sleep(0.5);
        $this->line("Loading version.json...");
        sleep(0.5);
        $this->line("Reading version.json...");
        sleep(0.5);
        $this->line("[INFO] Rex Tracker Version : " . $json_version['build-info']['rex-version']);
        $this->line("[INFO] RexAPI Version : " . $json_version['build-info']['rexapi-version']);
        $this->line("[INFO] Build ReleaseDate : " . $json_version['build-info']['ReleaseDate']);
        sleep(0.5);
        $this->line("Closing version.json...");
        sleep(0.5);
        $this->line("Loading config.json...");
        sleep(0.5);
        $this->line("Reading config.json...");
        sleep(0.5);
        $this->line("[INFO] Rex Tracker Config - Server URL : " . $json_config['config']['rex-tracker']['ServerUrl']);
        $this->line("[INFO] Rex Tracker Config - Theme : " . $json_config['config']['rex-tracker']['Theme']);
        $this->line("[INFO] Rex Tracker Config - ServerAdminCreated : " . $json_config['config']['rex-tracker']['ServerAdminCreated']);
        $this->line("");
        $this->line("[INFO] RexAPI Config - Format Type : " . $json_config['config']['rex-api']['FormatType']);
        $this->line("[INFO] RexAPI Config - DB Connection : " . $json_config['config']['rex-api']['DbConnection']);
        $this->line("[INFO] RexAPI Config - DB Tmp Table : " . $json_config['config']['rex-api']['DbTmpTable']);

        sleep(0.5);
        $this->line("Closing config.json...");
        sleep(0.5);
        $this->line("Removing Tmp Data...");
        $this->info("Removing Tmp Data... DONE!");
        $this->line("");
        if($error > 0) {
          $this->line("======================================================");
          $this->line("");
          $this->line($error . " Errors Occured");
          $this->line("Errors Occured In This Data");
          foreach($error_array as $erroritem) {
            $this->line("   - $erroritem");
          }
          $this->line("");
          $this->line("======================================================");
          $this->line("");
        }
        $this->info("RexAPI test completed successfuly!");
      }



    }
}

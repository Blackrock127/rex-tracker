<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Tribe;
use App\User;
use App\Notifications;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class rex extends Command
{
    protected $signature = 'rex {rexcommand?} {options?} {options1?} {options2?}';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function run( InputInterface $input, OutputInterface $output )
    {
        // Set extra colors.
        // The most problem is $output->getFormatter() don't work...
        // So create new formatter to add extra color.

        $formatter = new OutputFormatter( $output->isDecorated() );
        $formatter->setStyle( 'red', new OutputFormatterStyle( 'red', 'black' ) );
        $formatter->setStyle( 'green', new OutputFormatterStyle( 'green', 'black' ) );
        $formatter->setStyle( 'yellow', new OutputFormatterStyle( 'yellow', 'black' ) );
        $formatter->setStyle( 'blue', new OutputFormatterStyle( 'blue', 'black' ) );
        $formatter->setStyle( 'magenta', new OutputFormatterStyle( 'magenta', 'black' ) );
        $formatter->setStyle( 'yellow-blue', new OutputFormatterStyle( 'yellow', 'blue' ) );
        $output->setFormatter( $formatter );

        $result = parent::run( $input, $output );
    }

    public function handle()
    {
        //$this->line("[RexTracker] line : RexAPI failed to get config.json!");

        //$this->line("[RexTracker] line : " . $this->argument('rexcommand') . " is not a Rex Tracker Command!");

        $config = storage_path() . "/config.json";
        $json_config = json_decode(file_get_contents($config), true);
        $has_server_admin = $json_config['config']['rex-tracker']['ServerAdminCreated'];

        if($has_server_admin == "false") {
          $this->line("");
          $this->error("===============================================================");
          $this->error("There is no admin created! Use setup command to make a new one!");
          $this->error("Please create a new one to use Rex Tracker Commands!           ");
          $this->error("===============================================================");
          $this->line("");
        } else {

          if($this->argument('rexcommand') == "") {
            $this->getOutput()->setDecorated(true);
            $this->line("===========================================");
            $this->line("RexTracker v4.01");
            $this->line("Here is a list of all Rex Tracker Commands!");
            $this->line(" ");
            $this->line("[help] Gets a list of all commands available.");
            $this->line("[theme] Change The Current Theme");
            $this->line("[users] Manage Users");
            $this->line("[notifications] Manage Notifications");
            $this->line("[dinonetwork] Manage The DinoNetwork");
            $this->line("[wipe] Wipes all data in database using softdelete");
            $this->line("===========================================");
          }

          if($this->argument('rexcommand') == "rexversion") {
              $this->line("[RexTracker] RexTracker v4.01");
          }

          if($this->argument('rexcommand') == "help") {
              $this->line(" ");
              $this->line("===========================================");
              $this->line("RexTracker v4.01");
              $this->line("Here is a list of all Rex Tracker Commands!");
              $this->line(" ");
              $this->line("[help] Gets a list of all commands available.");
              $this->line("[theme] Change The Current Theme");
              $this->line("[users] Manage Users");
              $this->line("[notifications] Manage Notifications");
              $this->line("[dinonetwork] Manage The DinoNetwork");
              $this->line("[wipe] Wipes all data in database using softdelete");
              $this->line("===========================================");
          }

          if($this->argument('rexcommand') == "createadmin") {
              $this->line("[RexTracker] Current Admin Is : {ADMIN_NAME}");
              $this->line("[RexTracker] line : Cannot Create New Admin!");
          }

          if($this->argument('rexcommand') == "theme") {
              if($this->argument('options') == "") {

              } else {
                $this->info('Created New Admin Successfuly!');
                $json_config['config']['rex-tracker']['Theme'] = $this->argument('options');
                $json = json_encode($json_config);
                file_put_contents(storage_path() . "/config.json", $json);
              }
              $this->line("Current Theme Is : ". $json_config['config']['rex-tracker']['Theme']);
              $this->line("Available Themes : Default,Extinction,Aberration");
          }

          if($this->argument('rexcommand') == "test") {
            $style = new OutputFormatterStyle('red', 'yellow', array('bold', 'blink'));
            $this->getOutput()->getFormatter()->setStyle('fire', $style);

            $this->output->writeln('<fire>foo</>');
            //$bar = $this->output->createProgressBar(5);
            //$bar->advance();
          }

          if($this->argument('rexcommand') == "users") {
            if($this->argument('options') == "") {
              $this->error("[RexTracker] line :  Please Select a management command!");
            }
            if($this->argument('options') == "get") {
              $users = User::get();
              foreach($users as $user) {
                $this->line("- " . $user->name . " (UUID : $user->id)");
              }
            }
          }

          if($this->argument('rexcommand') == "notifications") {
              if($this->argument('options') == "send-all") {
                $users = User::get();
                foreach($users as $user) {
                  $this->line("- " . $user->name . " (UUID : $user->id)");
                }
              }
              if($this->argument('options') == "send") {
                if($this->argument('options1') == "") {
                $this->line("[RexTracker] line : User UUID Argument Missing!");
                }
                else if($this->argument('options2') == "") {
                $this->line("[RexTracker] line : Notification Content Argument Missing!");
              } else {
                $notifications = new Notifications;
                $notifications->notification_owner = $this->argument('options1');
                $notifications->notification_sent_by = "Rex Tracker";
                $notifications->notification_content = $this->argument('options2');
                $notifications->save();
              }
            }
          }

          if($this->argument('rexcommand') == "survivors") {
              $this->line("[RexTracker] line : Unkown line!");
          }

          if($this->argument('rexcommand') == "tribe") {
              if($this->argument('options') == "get") {
                $tribes = Tribe::get();
                foreach($tribes as $tribe) {
                  $this->line("- " . $tribe->name . " (UUID : $tribe->id)");
                }
              }
              if($this->argument('options') == "create") {
                if($this->argument('options1') == "") {
                $this->line("[RexTracker] line : Tribe Name Argument Missing!");
                } else {
                  $tribe = new Tribe;
                  $tribe->name = $this->argument('options1');
                  $tribe->owner = "0o0-0o0-0o0-0o0";
                  $tribe->save();
                }
              }
              if($this->argument('options') == "delete") {
                  $this->line("[RexTracker] line : Cannot Delete Tribe Using ID=null!");
              }
          }

        }

        if($this->argument('rexcommand') == "setup") {
          $this->info('Created New Admin Successfuly!');
          $json_config['config']['rex-tracker']['ServerAdminCreated'] = "true";
          $json = json_encode($json_config);
          file_put_contents(storage_path() . "/config.json", $json);
        }
    }
}

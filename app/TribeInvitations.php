<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class TribeInvitations extends Model {
    use UuidModelTrait, SoftDeletes;

    protected $fillable = ['invitation_sent_by','invitation_owner','tribe_name'];
    protected $hidden = [];
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];
    public $incrementing = false;
    protected $table = "tribe_invitations";
}

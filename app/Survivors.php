<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Survivors extends Model {
  use UuidModelTrait, SoftDeletes;

  protected $fillable = ['owner','server', 'name', 'level', 'tid', 'level', 'health', 'stamina', 'oxygen', 'food', 'water', 'weight', 'melee', 'movement', 'fortitude', 'crafting'];
  protected $hidden = [];
  protected $dates = ['deleted_at', 'updated_at', 'created_at'];
  public $incrementing = false;
  protected $table = "survivors";
}

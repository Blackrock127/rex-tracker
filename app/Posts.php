<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Alsofronie\Uuid\UuidModelTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Posts extends Model {
    use UuidModelTrait, SoftDeletes;

    protected $fillable = ['created_by','creator_name','post_content','post_image'];
    protected $hidden = [];
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];
    public $incrementing = false;
    protected $table = "posts";
}

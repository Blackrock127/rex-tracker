![Scheme](public/pics/RexTracker.png)  
[![Build](https://img.shields.io/badge/Build-Success-green.svg?style=flat-square)]()
[![Issues](https://img.shields.io/badge/Issues-0-c62828.svg?style=flat-square)]()
[![RexAPI](https://img.shields.io/badge/RexAPI-v5.23-2196f3.svg?style=flat-square)]()
[![Release Date](https://img.shields.io/badge/Release%20Date-January%202018-green.svg?style=flat-square)]()

## About Rex Tracker
----------

Rex Tracker is an ARK: Survival Evolved Toolkit as a Web Application! It Includes a Taming Calculator, Breeding Calculator and Dino Network.

## How to install
----------

> **Requirements :**

> - Laravel 5.5
> - PHP
> - Apache 2
> - MySQL
>

    git pull --all

##Configuration
----------
Theme Configuration :

    php artisan rex:theme "Default"

Database Configuration :

    php artisan migrate

  Admin Configuration **REQUIRED**
> **Note :**
> This configuration is required to get access to the admin dashboard went you install Rex  Tracker if you do not configure the Rex Tracker Admin, Rex Tracker wont be able to execute any commands.



    php artisan rex:create_admin "Admin Username" "Admin Password" "Admin Email"

##Rex API
----------
![Scheme](public/pics/RexAPI.png)

Rex API is a powerful API that can control the entire website using json a few commands are available too. Rex API is used for the Android and Windows Version of Rex Tracker.

Version of Rex API (Command) :

    php artisan rexapi --version

The Rex API is currently Uncompleted and very Unstable!

## How to push and pull
----------

Push:

    git add .
    git commit -m "Commit Message"
    git push origin master
    git push -f origin master **FORCE PUSH ONLY**

Pull:

    git pull --all


##Patch Notes
----------

**Patch Notes of Rex Tracker v4.17**

- Added New Admin Management System

- Added Element Calculator

- Added Forge Calculator

- Added Mortar Calculator

- Added Turret Refill Timers

- Added Custom Timers

- Added Dino Network

- Added New Security System

- Fixed RexAPI

- Added new dashboard style

- Added Logs

- Fixed Many Bugs

- Added Optimization

- Added new features in RexAPI

- Added new Delete Warning Dialog

- Added Private Server Option

- Added Skip Creating Tribe Option

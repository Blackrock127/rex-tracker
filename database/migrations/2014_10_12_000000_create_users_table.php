<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->enum('role', ['admin', 'mod', 'premium', 'user'])->default('user');
            $table->string('picture')->default('default.png');
            $table->softDeletes()->nullable()->default(NULL);
            $table->boolean('forcechpwd')->default(FALSE);
            $table->string('first_login')->default("TRUE");
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}

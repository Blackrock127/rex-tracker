<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('timers', function (Blueprint $table) {
          $table->uuid('id');
          $table->primary('id');
          $table->string('name');
          $table->enum('type', ['turret', 'custom'])->default('custom');
          $table->enum('status', ['stopped', 'active'])->default('active');
          $table->timestamp('new_date');
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timers');
    }
}

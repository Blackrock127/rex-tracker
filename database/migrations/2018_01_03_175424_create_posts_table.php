<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
          $table->uuid('id');
          $table->primary('id');
          $table->uuid('created_by');
          $table->string('creator_name');
          $table->string('creator_picture')->default('default.png');
          $table->string('post_content');
          $table->string('post_image')->nullable()->default(NULL);
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

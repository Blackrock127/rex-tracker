<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurvivorsTable extends Migration
{
  public function up() {
      Schema::create('survivors', function (Blueprint $table) {
        $table->uuid('id');
        $table->primary('id');
        $table->uuid('owner');
        $table->string('name')->nullable()->default(NULL);
        $table->string('server')->default("Private");
        $table->unsignedInteger('level')->nullable()->default(1);
        $table->unsignedDecimal('health', 6, 2)->default("100.00");
        $table->unsignedDecimal('stamina', 6, 2)->default("100.00");
        $table->unsignedDecimal('oxygen', 6, 2)->default("100.00");
        $table->unsignedDecimal('food', 6, 2)->default("100.00");
        $table->unsignedDecimal('water', 6, 2)->default("100.00");
        $table->unsignedDecimal('weight', 6, 2)->default("100.00");
        $table->unsignedDecimal('melee', 6, 2)->default("0.00");
        $table->unsignedDecimal('movement', 6, 2)->default("100.00");
        $table->unsignedDecimal('fortitude', 6, 2)->default("0.00");
        $table->unsignedDecimal('crafting', 6, 2)->default("0.00");
        $table->string('has_tribe')->default("FALSE");
        $table->timestamps();
        $table->softDeletes()->nullable()->default(NULL);
      });
  }

  public function down()
  {
      Schema::dropIfExists('survivors');
  }
}

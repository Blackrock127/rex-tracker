<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTribesTable extends Migration
{
    public function up() {
        Schema::create('tribes', function (Blueprint $table) {
          $table->uuid('id');
          $table->primary('id');
          $table->string('name');
          $table->uuid('owner');
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    public function down()
    {
        Schema::dropIfExists('tribes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
          $table->uuid('id');
          $table->primary('id');
          $table->uuid('notification_owner');
          $table->string('notification_sent_by');
          $table->string('notification_content');
          $table->enum('notification_type', ['TribeNotifiaction', 'Default'])->default('Default');
          $table->string('custom_link')->nullable()->default(NULL);
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}

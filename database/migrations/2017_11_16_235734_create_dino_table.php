<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDinoTable extends Migration {
    public function up() {
        Schema::create('dinos', function (Blueprint $table) {
          $table->uuid('id');
          $table->primary('id');
          $table->string('dino_picture')->default('default.png');
          $table->uuid('owner');
          $table->string('tamed_by')->nullable()->default(NULL);
          $table->string('created_by')->nullable()->default(NULL);
          $table->string('name')->nullable()->default(NULL);
          $table->string('type')->nullable()->default(NULL);
          $table->unsignedInteger('level')->nullable()->default(1);
          $table->enum('gender', ['male', 'female'])->nullable()->default(NULL);
          $table->unsignedDecimal('health', 20, 2)->default("100.00");
          $table->unsignedDecimal('stamina', 20, 2)->default("100.00");
          $table->unsignedDecimal('oxygen', 20, 2)->default("100.00");
          $table->unsignedDecimal('food', 20, 2)->default("100.00");
          $table->unsignedDecimal('water', 20, 2)->default("100.00");
          $table->unsignedDecimal('weight', 20, 2)->default("100.00");
          $table->unsignedDecimal('melee', 20, 2)->default("0.00");
          $table->unsignedDecimal('movement', 20, 2)->default("100.00");
          $table->unsignedDecimal('torpidity', 20, 2)->default("0.00");
          $table->unsignedInteger('baselevel')->nullable()->default(1);
          $table->unsignedDecimal('basehealth', 20, 2)->nullable()->default(NULL);
          $table->unsignedDecimal('basestamina', 20, 2)->nullable()->default(NULL);
          $table->unsignedDecimal('baseoxygen', 20, 2)->nullable()->default(NULL);
          $table->unsignedDecimal('basefood', 20, 2)->nullable()->default(NULL);
          $table->unsignedDecimal('basewater', 20, 2)->nullable()->default(NULL);
          $table->unsignedDecimal('baseweight', 20, 2)->nullable()->default(NULL);
          $table->unsignedDecimal('basemelee', 20, 2)->nullable()->default(NULL);
          $table->unsignedDecimal('basemovement', 20, 2)->nullable()->default(NULL);
          $table->unsignedDecimal('imprinting', 20, 2)->default("0.00");
          $table->unsignedInteger('paternal_mut')->default("0");
          $table->unsignedInteger('maternal_mut')->default("0");
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    public function down() {
        Schema::dropIfExists('dinos');
    }
}

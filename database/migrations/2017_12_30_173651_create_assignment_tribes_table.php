<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentTribesTable extends Migration
{
    public function up() {
        Schema::create('assignment_tribes', function (Blueprint $table) {
          $table->uuid('id');
          $table->primary('id');
          $table->string('name');
          $table->uuid('sid');
          $table->uuid('tid');
          $table->enum('role', ['Owner', 'Admin', 'Member'])->default('Member');
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    public function down()
    {
        Schema::dropIfExists('assignment_tribes');
    }
}

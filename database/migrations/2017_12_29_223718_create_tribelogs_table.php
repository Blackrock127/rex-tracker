<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTribelogsTable extends Migration
{
    public function up() {
        Schema::create('tribelogs', function (Blueprint $table) {
          $table->uuid('id');
          $table->primary('id');
          $table->uuid('owner');
          $table->string('survivor_name');
          $table->string('log_message');
          $table->timestamps();
          $table->softDeletes()->nullable()->default(NULL);
        });
    }

    public function down()
    {
        Schema::dropIfExists('tribelogs');
    }
}

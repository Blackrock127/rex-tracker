<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDinoNetworkProfileTable extends Migration
{
    public function up()
    {
        Schema::create('dino_network_profile', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('sid');
            $table->string('name');
            $table->string('picture')->default('default.png');
            $table->string('tag');
            $table->timestamps();
            $table->softDeletes()->nullable()->default(NULL);
        });
    }

    public function down()
    {
        Schema::dropIfExists('dino_network_profile');
    }
}

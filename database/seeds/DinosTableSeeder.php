<?php

use Illuminate\Database\Seeder;
use App\Dinos;

class DinosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dino = new Dinos;

        $dino->owner = "c601016e-8547-4e7c-a759-f3ddbff269a9";
        $dino->name = str_random(10);
        $dino->gender = "female";
        $dino->level = mt_rand(1, 150);

        $dino->save();

    }
}

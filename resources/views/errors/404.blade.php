
<!doctype html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rex Tracker</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="http://rex-tracker.wcksoft.com/pics/logo.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="http://rex-tracker.wcksoft.com/css/vendor.css">
        <!-- Theme initialization -->
        <script>
            var themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) :
            {};
            var themeName = themeSettings.themeName || '';
            if (themeName)
            {
                document.write('<link rel="stylesheet" href="http://rex-tracker.wcksoft.com/css/app-' + themeName + '.css">');
            }
            else
            {
                document.write('<link rel="stylesheet" href="http://rex-tracker.wcksoft.com/css/app.css">');
            }
        </script>
    </head>

    <body>
        <div class="app blank sidebar-opened">
            <article class="content">
                <div class="error-card global">
                    <div class="error-title-block">
                        <h1 class="error-title">404</h1>
                        <h2 class="error-sub-title"> Sorry, page not found </h2>
                    </div>
                    <div class="error-container" style="margin-top: 12px;">
                        <a class="btn btn-primary" href="/dashboard">
                  	    	<i class="fa fa-angle-left"></i>
                  	    	Back to Dashboard
                  	    </a>
                    </div>
                </div>
            </article>
        </div>
        <!-- Reference block for JS -->
        <div class="ref" id="ref">
            <div class="color-primary"></div>
            <div class="chart">
                <div class="color-primary"></div>
                <div class="color-secondary"></div>
            </div>
        </div>
        <script src="http://rex-tracker.wcksoft.com/js/vendor.js"></script>
        <script src="http://rex-tracker.wcksoft.com/js/app.js"></script>
    </body>

</html>

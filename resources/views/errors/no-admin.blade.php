
<!doctype html>
<html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Rex Tracker</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="icon" href="http://rex-tracker.wcksoft.com/pics/logo.png">
        <!-- Theme initialization -->
        <link rel="stylesheet" href="http://rex-tracker.wcksoft.com/css/vendor.css">
        <link rel="stylesheet" href="http://rex-tracker.wcksoft.com/css/app.css">
        </script>
    </head>

    <body>
        <div class="app blank sidebar-opened" style="background-color: #af2525;">
            <article class="content">
                <div class="error-card global">
                    <div class="error-title-block">
                        <h1 class="error-title" style="font-size: 84px;">Warning!</h1>
                        <h2 class="error-sub-title"> There is no admin created! Use setup command to make a new one! </h2>
                    </div>
                </div>
            </article>
        </div>
        <!-- Reference block for JS -->
        <div class="ref" id="ref">
            <div class="color-primary"></div>
            <div class="chart">
                <div class="color-primary"></div>
                <div class="color-secondary"></div>
            </div>
        </div>
        <script src="http://rex-tracker.wcksoft.com/js/vendor.js"></script>
        <script src="http://rex-tracker.wcksoft.com/js/app.js"></script>
    </body>

</html>

<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title> Rex Tracker </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="css/vendor.css">
  <!-- Theme initialization -->
  <script>
            var themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) :
            {};
            var themeName = themeSettings.themeName || '';
            if (themeName)
            {
                document.write('<link rel="stylesheet" id="theme-style" href="css/app-' + themeName + '.css">');
            }
            else
            {
                document.write('<link rel="stylesheet" id="theme-style" href="css/app.css">');
            }
        </script>
 </head>
 <body>
  <div class="auth" style="background-color: #f0f3f6;">
   <div class="auth-container">
    <div class="card">
     <header class="auth-header">
      <h1 class="auth-title">
        <img src="/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -16px;">
        TRACKER
      </h1>
     </header>
     <div class="auth-content">
      <form id="login-form" action="/setup/survivor" method="POST" novalidate="">
       {{ csrf_field() }}
       @if ($errors->any())
       @foreach ($errors->all() as $error)
       <center><p class="text-danger">{{ $error }}</p></center>
       @endforeach
       @endif
       <div class="form-group">
        <input type="text" class="form-control" id="survivor_name"  name="survivor_name" placeholder="Survivor Name">
       </div>
       <div class="form-group"> <select class="form-control" id="server_type" name="server_type">
                    <option id="null" disabled selected>Choose server type</option>
                    <option id="offical_input">Official</option>
                    <option id="delicated_input" >Delicated</option>
       						</select>
       </div>
       <div id="offical" style="display:none">
         <br>
         <input id="server_number" type="number" name="server_number" class="form-control" placeholder="Server Number" style="border-radius: 4px;"/>
       </div>
       <div id="offical1" style="display:none">
         <br>
         <select class="form-control" id="server_map"  name="server_map" style="border-radius: 4px;">
                     <option disabled selected>Choose map</option>
                     <option style="color: #9269e6;">Aberration</option>
                     <option>The Island</option>
                     <option>Scorched Earth</option>
                     <option>The Center</option>
                     <option>Ragnarok</option>
           </select>
           <br>
           <select class="form-control" id="server_platform" name="server_platform">
                        <option id="null" disabled selected>Choose Server Platform</option>
                        <option id="offical_input">PC</option>
                        <option id="delicated_input" >PS4</option>
                        <option id="offical_input">Xbox</option>
           </select>
       </div>
       <div id="delicated" style="display:none">
         <br>
         <input id="server_name" type="text" name="server_name" class="form-control" placeholder="Server Name"/>
         <br>
         <select class="form-control" id="delicated_server_platform" name="delicated_server_platform">
                      <option id="null" disabled selected>Choose Server Platform</option>
                      <option id="offical_input">PC</option>
                      <option id="delicated_input" >PS4</option>
                      <option id="offical_input">Xbox</option>
         </select>
       </div>
       <br>
       <div class="form-group">
        <label for="remember"> <input class="checkbox" id="remember" name="remember" type="checkbox" onclick="toggleSelect()"> <span>I dont want to show my server</span> </label>
       </div>
       <div>
       <br>
       <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Create Survivor</button>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script>
    window.onload = toggleSelect();

    function toggleSelect()
    {
      var isChecked = document.getElementById("remember").checked;
      document.getElementById("server_type").disabled = isChecked;
      document.getElementById("server_name").disabled = isChecked;
      document.getElementById("server_map").disabled = isChecked;
      document.getElementById("server_number").disabled = isChecked;
      document.getElementById("server_platform").disabled = isChecked;
      document.getElementById("delicated_server_platform").disabled = isChecked;
    }
    </script>
    <script>
      $("#server_type").change(function() {
        if ($("#offical_input").is(":selected")) {
          $("#offical").show('slow');
          $("#offical1").show('slow');
          $("#delicated").hide('slow');
        } else {
          $("#offical").hide('slow');
          $("#offical1").hide('slow');
          $("#delicated").show('slow');
        }
        if ($("#null").is(":selected")) {
          $("#offical").hide();
          $("#offical1").hide();
          $("#delicated").hide();
        }
      }).trigger('change');
    </script>

  <script src="js/vendor.js"></script>
  <script src="js/app.js"></script>
 </body>
</html>

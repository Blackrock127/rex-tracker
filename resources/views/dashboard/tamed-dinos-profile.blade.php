<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Rex Tracker</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{$server_url}}/pics/logo.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="{{$server_url}}/css/vendor.css">
  <!-- Theme initialization -->
  <link rel="stylesheet" href="{{$server_url}}/css/app.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 </head>
 <body>
  <div class="main-wrapper">
   <div class="app" id="app">
    <header class="header">
     <div class="header-block header-block-collapse hidden-lg-up">
      <button class="collapse-btn" id="sidebar-collapse-btn"> <i class="fa fa-bars"></i> </button>
     </div>
     <div class="header-block header-block-nav">
      <ul class="nav-profile">
       <li class="notifications new"> <a href="" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <sup> <span class="counter"></span> </sup> </a>
        <div class="dropdown-menu notifications-dropdown-menu">
          <ul class="notifications-container">
            <center>
                        <br>
                        <i class="fa fa-bell-o fa-3x" aria-hidden="true"></i><br><br>
                        <p style="font-size:16px;">You do not have any notifications</p><br>
                        <br>
            </center>
          </ul>
         <!--<footer>
          <ul>
           <li> <a href=""> View All </a> </li>
          </ul>
         </footer>
       </div> </li>-->
       @if (Auth::guest())
       <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
         <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/default.png')">
         </div> <span> Guest </span> </a>
        <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
         <a class="dropdown-item" href="/login"> <i class="fa fa-sign-in icon"></i> Login </a>
         <a class="dropdown-item" href="/register"> <i class="fa fa-user icon"></i> Register </a>
        </div> </li>
        @else
         <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
           <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/{{ Auth::user()->picture }}')">
           </div> <span> {{ Auth::user()->name }} </span> </a>
          <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
           <a class="dropdown-item" href="/dashboard"> <i class="fa fa-tachometer icon"></i> Dashboard </a>
           <a class="dropdown-item" href="/dino-network"> <i class="fa fa-user icon"></i> Profile </a>
           <a class="dropdown-item" href="/settings"> <i class="fa fa-gear icon"></i> Settings </a>
           <div class="dropdown-divider"></div>
           <a class="dropdown-item" href="/logout"> <i class="fa fa-power-off icon"></i> Logout </a>
          </div> </li>
         @endif
      </ul>
     </div>
    </header>
    <aside class="sidebar">
     <div class="sidebar-container">
      <div class="sidebar-header">
       <div class="brand" style="padding-top: 17px;line-height: 34px;">
          <img src="/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -20px;">
          <p style="font-size: 25px;display: inline;">TRACKER<p>
       </div>
      </div>
      <nav class="menu">
       <ul class="nav metismenu" id="sidebar-menu">
         <li> <a href="/dashboard"> <i class="fa fa-tachometer"></i> Dashboard </a> </li>
         <li class="active"> <a href="/tamed-dinos"> <i class="fa fa-list"></i> Tamed Dinos</a> </li>
         <li> <a href="/tribe-logs"> <i class="fa fa-history"></i> Tribe Logs</a> </li>
         <br>
         <li>
           <a href="">
             <i class="fa fa-clock-o"></i>Timers
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <li> <a href="/turret-timers"> <i class="fa fa-clock-o"></i> Turret Refill Timer</a> </li>
             <li> <a href="/custom-timers"> <i class="fa fa-clock-o"></i> Custom Timers</a> </li>
           </ul>
         </li>
         <li>
           <a href="">
             <i class="fa fa-calculator"></i>Calculators
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <!--<li> <a href="/taming-calculator"> <i class="fa fa-calculator"></i> Taming Calculator</a> </li>
             <li> <a href="/breeding-calculator"> <i class="fa fa-calculator"></i> Breeding Calculator</a> </li>
             <li> <a href="/stats-calculator"> <i class="fa fa-calculator"></i> Stats Calculator</a> </li>-->
             <li> <a href="/element-calculator"> <i class="fa fa-calculator"></i> Element Calculator</a> </li>
             <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Mortar Calculator</a> </li>
             <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Forge Calculator</a> </li>
           </ul>
         </li>
         <br>
         <li> <a href="/dino-network"> <i class="fa fa-globe"></i> Dino Network</a> </li>
         <li> <a href="/patch-notes"> <i class="fa fa-arrow-circle-down"></i> Patch Notes</a> </li>
        <!--<li> <a href=""> <i class="fa fa-th-large"></i> Items Manager <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="items-list.html"> Items List </a> </li>
          <li> <a href="item-editor.html"> Item Editor </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-bar-chart"></i> Charts <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="charts-flot.html"> Flot Charts </a> </li>
          <li> <a href="charts-morris.html"> Morris Charts </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-table"></i> Tables <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="static-tables.html"> Static Tables </a> </li>
          <li> <a href="responsive-tables.html"> Responsive Tables </a> </li>
         </ul> </li>
        <li> <a href="forms.html"> <i class="fa fa-pencil-square-o"></i> Forms </a> </li>
        <li> <a href=""> <i class="fa fa-desktop"></i> UI Elements <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="buttons.html"> Buttons </a> </li>
          <li> <a href="cards.html"> Cards </a> </li>
          <li> <a href="typography.html"> Typography </a> </li>
          <li> <a href="icons.html"> Icons </a> </li>
          <li> <a href="grid.html"> Grid </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-file-text-o"></i> Pages <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="login.html"> Login </a> </li>
          <li> <a href="signup.html"> Sign Up </a> </li>
          <li> <a href="reset.html"> Reset </a> </li>
          <li> <a href="error-404.html"> Error 404 App </a> </li>
          <li> <a href="error-404-alt.html"> Error 404 Global </a> </li>
          <li> <a href="error-500.html"> Error 500 App </a> </li>
          <li> <a href="error-500-alt.html"> Error 500 Global </a> </li>
         </ul> </li>
        <li> <a href="https://github.com/modularcode/modular-admin-html"> <i class="fa fa-github-alt"></i> Theme Docs </a> </li>-->
       </ul>
      </nav>
     </div>
     <footer class="sidebar-footer">
      <ul class="nav metismenu" id="customize-menu">
       <li>
        <ul>
         <li class="customize">
          <div class="customize-item">
           <div class="row customize-header">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-4">
             <label class="title">fixed</label>
            </div>
            <div class="col-xs-4">
             <label class="title">static</label>
            </div>
           </div>
           <div class="row hidden-md-down">
            <div class="col-xs-4">
             <label class="title">Sidebar:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value="sidebar-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Header:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value="header-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Footer:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value="footer-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value=""> <span></span> </label>
            </div>
           </div>
          </div>
          <div class="customize-item">
           <ul class="customize-colors">
            <li> <span class="color-item color-red" data-theme="red"></span> </li>
            <li> <span class="color-item color-orange" data-theme="orange"></span> </li>
            <li> <span class="color-item color-green active" data-theme=""></span> </li>
            <li> <span class="color-item color-seagreen" data-theme="seagreen"></span> </li>
            <li> <span class="color-item color-blue" data-theme="blue"></span> </li>
            <li> <span class="color-item color-purple" data-theme="purple"></span> </li>
           </ul>
          </div> </li>
        </ul> </li>
      </ul>
     </footer>
    </aside>
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <article class="content dashboard-page" >
     <section class="section">
      <div class="row">
       <div class="col col-xs-12 col-sm-12 col-md-4 col-xl-4 stats-col">
         <div class="card" data-exclude="xs">
           <div class="card-block" style="">
             <h2>{{$dinos->name}} - {{$dinos->type}}</h2>
             <p>Level 150</p>
             <center>
               <img src="{{$server_url}}/pics/src/tamed-dinos/{{$dinos->dino_picture}}" width="460" height="345" style="width: 100%;height: auto;">
               <hr>
               <h5>Tamed By: {{$dino->tamed_by}}</h5>
               <br>
               <a class="btn btn-pill-left btn-primary" href="/tamed-dinos/{{$dinos->id}}/edit"><i class="fa fa-pencil"></i> Edit</a><a class="btn btn-pill-right btn-danger" href="/tamed-dinos/{{$dinos->id}}/delete"><i class="fa fa-trash-o"></i> Delete</a>
             </center>
           </div>
         </div>
       </div>
       <div class="col col-xs-12 col-sm-12 col-md-8 col-xl-8 stats-col">
        <div class="card" data-exclude="xs">
         <div class="card-block" style="">
           <center>
           <span style="display: block;font-size: 23px;font-weight: bold;color: #85ce36;">Dino Stats</span>
           <div class="row">
                <div class="col-sm-6">
                  <ul class="item-list">
                        <li><img src="{{$server_url}}/pics/Icons/80px-Health.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Health : {{$dinos->health}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Stamina.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Stamina : {{$dinos->stamina}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Oxygen.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Oxygen : {{$dinos->oxygen}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Food.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Food : {{$dinos->food}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Weight.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Weight : {{$dinos->weight}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Melee_Damage.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Melee Damage : {{$dinos->melee}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Movement_Speed.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Movement Speed : {{$dinos->movement}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Torpidity.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Torpidity : {{$dinos->torpidity}}</li>
                  </ul>
                </div>
                <div class="col-sm-6">
                  <ul class="item-list">
                        <li><img src="{{$server_url}}/pics/Icons/80px-Health.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Base Health : {{$dinos->basehealth}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Oxygen.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Base Stamina : {{$dinos->basestamina}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Water.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Base Oxygen : {{$dinos->baseoxygen}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Melee_Damage.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Base Food : {{$dinos->basefood}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Fortitude.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Base Weight : {{$dinos->baseweight}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Fortitude.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Base Melee Damage : {{$dinos->basemelee}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Fortitude.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Base Movement Speed : {{$dinos->basemovement}}</li>
                        <li><img src="{{$server_url}}/pics/Icons/80px-Fortitude.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Base Torpidity : {{$dinos->torpidity}}</li>
                  </ul>
                </div>
                <span style="display: block;font-size: 23px;font-weight: bold;color: #85ce36;">Mutations & Imprints</span>
                <div class="row">
                <div class="col-sm-6">
                  <ul class="item-list">
                        <li><img src="{{$server_url}}/pics/Icons/female.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Maternal Mutation : {{$dinos->maternal_mut}}/20</li>
                  </ul>
                </div>
                <div class="col-sm-6">
                  <ul class="item-list">
                        <li><img src="{{$server_url}}/pics/Icons/male.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Paternal Mutation : {{$dinos->paternal_mut}}/20</li>
                  </ul>
                </div>
                <h5>Imprint :{{$dinos->imprinting}}%</h5>
              </div>
            </div>
          </center>
         </div>
        </div>
       </div>
      </div>
     </section>
     <section class="section map-tasks">
     </section>
    </article>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script src="{{$server_url}}/js/vendor.js"></script>
  <script src="{{$server_url}}/js/app.js"></script>
 </body>
</html>

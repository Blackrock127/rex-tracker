<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Rex Tracker</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{$server_url}}/pics/logo.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="{{$server_url}}/css/vendor.css">
  <!-- Theme initialization -->
  <link rel="stylesheet" href="{{$server_url}}/css/app.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 </head>
 <body>
  <div class="main-wrapper">
   <div class="app" id="app">
    <header class="header">
     <div class="header-block header-block-collapse hidden-lg-up">
      <button class="collapse-btn" id="sidebar-collapse-btn"> <i class="fa fa-bars"></i> </button>
     </div>
     <div class="header-block header-block-nav">
      <ul class="nav-profile">
       <li class="notifications new"> <a href="" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <sup> <span class="counter"></span> </sup> </a>
        <div class="dropdown-menu notifications-dropdown-menu">
         <ul class="notifications-container">

         </ul>
         <footer>
          <ul>
           <li> <a href=""> View All </a> </li>
          </ul>
         </footer>
        </div> </li>
        @if (Auth::guest())
        <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/default.png')">
          </div> <span> Guest </span> </a>
         <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
          <a class="dropdown-item" href="/login"> <i class="fa fa-sign-in icon"></i> Login </a>
          <a class="dropdown-item" href="/register"> <i class="fa fa-user icon"></i> Register </a>
         </div> </li>
         @else
          <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/{{ Auth::user()->picture }}')">
            </div> <span> {{ Auth::user()->name }} </span> </a>
           <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
            <a class="dropdown-item" href="/dashboard"> <i class="fa fa-tachometer icon"></i> Dashboard </a>
            <a class="dropdown-item" href="/dino-network"> <i class="fa fa-user icon"></i> Profile </a>
            <a class="dropdown-item" href="/settings"> <i class="fa fa-gear icon"></i> Settings </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/logout"> <i class="fa fa-power-off icon"></i> Logout </a>
           </div> </li>
          @endif
      </ul>
     </div>
    </header>
    <aside class="sidebar">
     <div class="sidebar-container">
      <div class="sidebar-header">
       <div class="brand" style="padding-top: 17px;line-height: 34px;">
          <img src="/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -20px;">
          <p style="font-size: 25px;display: inline;">TRACKER<p>
       </div>
      </div>
      <nav class="menu">
       <ul class="nav metismenu" id="sidebar-menu">
         <li> <a href="/dashboard"> <i class="fa fa-tachometer"></i> Dashboard </a> </li>
         <li class="active"> <a href="/tamed-dinos"> <i class="fa fa-list"></i> Tamed Dinos</a> </li>
         <li> <a href="/tribe-logs"> <i class="fa fa-history"></i> Tribe Logs</a> </li>
         <br>
         <li>
           <a href="">
             <i class="fa fa-clock-o"></i>Timers
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <li> <a href="/turret-timers"> <i class="fa fa-clock-o"></i> Turret Refill Timer</a> </li>
             <li> <a href="/custom-timers"> <i class="fa fa-clock-o"></i> Custom Timers</a> </li>
           </ul>
         </li>
         <li>
           <a href="">
             <i class="fa fa-calculator"></i>Calculators
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <li> <a href="/taming-calculator"> <i class="fa fa-calculator"></i> Taming Calculator</a> </li>
             <li> <a href="/breeding-calculator"> <i class="fa fa-calculator"></i> Breeding Calculator</a> </li>
             <li> <a href="/stats-calculator"> <i class="fa fa-calculator"></i> Stats Calculator</a> </li>
             <li> <a href="/element-calculator"> <i class="fa fa-calculator"></i> Element Calculator</a> </li>
             <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Forge Calculator</a> </li>
           </ul>
         </li>
         <br>
         <li>
           <a href="" style="color: #fe974b;">
             <i class="fa fa-tachometer"></i>Admin Dashboard
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <li> <a href="/admin-dashboard"> <i class="fa fa-tachometer"></i> Admin Dashboard</a> </li>
             <li> <a href="/admin-dashboard/users"> <i class="fa fa-user"></i> Manage Users</a> </li>
             <li> <a href="/admin-dashboard/tribes"> <i class="fa fa-list"></i> Manage Tribes</a> </li>
             <li> <a href="/admin-dashboard/management"> <i class="fa fa-wrench"></i> Management Tools</a> </li>
           </ul>
         </li>
         <br>
         <li> <a href="/dino-network"> <i class="fa fa-globe"></i> Dino Network</a> </li>
         <li> <a href="/patch-notes"> <i class="fa fa-arrow-circle-down"></i> Patch Notes</a> </li>
        <!--<li> <a href=""> <i class="fa fa-th-large"></i> Items Manager <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="items-list.html"> Items List </a> </li>
          <li> <a href="item-editor.html"> Item Editor </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-bar-chart"></i> Charts <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="charts-flot.html"> Flot Charts </a> </li>
          <li> <a href="charts-morris.html"> Morris Charts </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-table"></i> Tables <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="static-tables.html"> Static Tables </a> </li>
          <li> <a href="responsive-tables.html"> Responsive Tables </a> </li>
         </ul> </li>
        <li> <a href="forms.html"> <i class="fa fa-pencil-square-o"></i> Forms </a> </li>
        <li> <a href=""> <i class="fa fa-desktop"></i> UI Elements <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="buttons.html"> Buttons </a> </li>
          <li> <a href="cards.html"> Cards </a> </li>
          <li> <a href="typography.html"> Typography </a> </li>
          <li> <a href="icons.html"> Icons </a> </li>
          <li> <a href="grid.html"> Grid </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-file-text-o"></i> Pages <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="login.html"> Login </a> </li>
          <li> <a href="signup.html"> Sign Up </a> </li>
          <li> <a href="reset.html"> Reset </a> </li>
          <li> <a href="error-404.html"> Error 404 App </a> </li>
          <li> <a href="error-404-alt.html"> Error 404 Global </a> </li>
          <li> <a href="error-500.html"> Error 500 App </a> </li>
          <li> <a href="error-500-alt.html"> Error 500 Global </a> </li>
         </ul> </li>
        <li> <a href="https://github.com/modularcode/modular-admin-html"> <i class="fa fa-github-alt"></i> Theme Docs </a> </li>-->
       </ul>
      </nav>
     </div>
     <footer class="sidebar-footer">
      <ul class="nav metismenu" id="customize-menu">
       <li>
        <ul>
         <li class="customize">
          <div class="customize-item">
           <div class="row customize-header">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-4">
             <label class="title">fixed</label>
            </div>
            <div class="col-xs-4">
             <label class="title">static</label>
            </div>
           </div>
           <div class="row hidden-md-down">
            <div class="col-xs-4">
             <label class="title">Sidebar:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value="sidebar-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Header:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value="header-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Footer:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value="footer-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value=""> <span></span> </label>
            </div>
           </div>
          </div>
          <div class="customize-item">
           <ul class="customize-colors">
            <li> <span class="color-item color-red" data-theme="red"></span> </li>
            <li> <span class="color-item color-orange" data-theme="orange"></span> </li>
            <li> <span class="color-item color-green active" data-theme=""></span> </li>
            <li> <span class="color-item color-seagreen" data-theme="seagreen"></span> </li>
            <li> <span class="color-item color-blue" data-theme="blue"></span> </li>
            <li> <span class="color-item color-purple" data-theme="purple"></span> </li>
           </ul>
          </div> </li>
        </ul> </li>
      </ul>
     </footer>
    </aside>
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <article class="content dashboard-page" >
     <section class="section">
       <div class="col col-xs-12 col-sm-12 col-md-3 col-xl-3 stats-col">

       </div>
       <div class="col col-xs-12 col-sm-12 col-md-3 col-xl-3 stats-col">

       </div>
     </section>
     <section class="section">
      <div class="row">
       <div class="col col-xs-12 col-sm-12 col-md-3 col-xl-3 stats-col">

       </div>
       <div class="col col-xs-12 col-sm-12 col-md-6 col-xl-6 stats-col">
        <div class="card" data-exclude="xs">
         <div class="card-block" style="">
           <form action="/tamed-dinos/{{$dino_data->id}}/edit-save" method="POST" novalidate="" enctype="multipart/form-data">
             {{ csrf_field() }}
            <center><h5>Edit {{$dino_data->name}} - {{$dino_data->type}}</h5></center>
            @if ($errors->any())
            @foreach ($errors->all() as $error)
            <center><p class="text-danger">{{ $error }}</p></center>
            @endforeach
            @endif
            <span style="display: block;font-size: 23px;font-weight: bold;color: #85ce36;">Basic Information</span><br>
            <div class="file-upload">
              <div class="file-select">
                <div class="file-select-button" id="fileName">Change Dino Picture</div>
                <div class="file-select-name" id="noFile">No file chosen...</div>
                <input type="file" name="dinoPicture" id="dinoPicture" multiple accept='image/*'>
              </div>
              <br>
            </div>
            </label>
            <input type="text" class="form-control underlined" name="name" id="name" placeholder="Name" value="{{$dino_data->name}}" required><br>
            <input type="text" class="form-control underlined" name="dino_type" id="dino_type" placeholder="Dino Type" value="{{$dino_data->type}}" required><br>
            <input type="number" class="form-control underlined" name="level" id="level" placeholder="Level" value="{{$dino_data->level}}" required><br>
            <div class="form-group">
              <label class="control-label">Gender</label>
              <div>
                      <label>
                          @if($dino_data->gender == "female")
                          <input class="radio" name="inline-radios" type="radio" readonly checked>
                          @else
                          <input class="radio" name="inline-radios" type="radio" readonly>
                          @endif
                          <span>Female</span>
                      </label>
                      <label>
                          @if($dino_data->gender == "male")
                          <input class="radio" name="inline-radios" type="radio" readonly checked>
                          @else
                          <input class="radio" name="inline-radios" type="radio" readonly>
                          @endif
                          <span>Male</span>
                      </label>
              </div>
            </div>
            <span style="display: block;font-size: 23px;font-weight: bold;color: #85ce36;">Stats</span><br>
            <div class="row">
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="health" id="health" placeholder="Health" value="{{$dino_data->health}}">
                </div>
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="base_health" id="base_health" placeholder="Base Health" value="{{$dino_data->base_health}}">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="stamina" id="stamina" placeholder="Stamina" value="{{$dino_data->stamina}}">
                </div>
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="base_stamina" id="base_stamina" placeholder="Base Stamina" value="{{$dino_data->base_stamina}}">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="oxygen" id="oxygen"  placeholder="Oxygen" value="{{$dino_data->oxygen}}">
                </div>
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="base_oxygen" id="base_oxygen" placeholder="Base Oxygen" value="{{$dino_data->base_oxygen}}">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="food" id="food" placeholder="Food" value="{{$dino_data->food}}">
                </div>
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="base_food" id="base_food" placeholder="Base Food" value="{{$dino_data->base_food}}">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="weight" id="weight" placeholder="Weight" value="{{$dino_data->weight}}">
                </div>
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="base_weight" id="base_weight" placeholder="Base Weight" value="{{$dino_data->base_weight}}">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="melee" id="melee" placeholder="Melee Damage" value="{{$dino_data->melee}}">
                </div>
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="base_melee" id="base_melee" placeholder="Base Melee Damage" value="{{$dino_data->base_melee}}">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="movement" id="movement" placeholder="Movement Speed" value="{{$dino_data->movement}}">
                </div>
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="base_movement" id="base_movement" placeholder="Base Movement Speed" value="{{$dino_data->base_movement}}">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="torpor" id="torpor" placeholder="Torpidity" value="{{$dino_data->torpidity}}">
                </div>
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="base_torpor" id="base_torpor" placeholder="Base Torpidity" value="{{$dino_data->torpidity}}">
                </div>
            </div>
            <br>
            <span style="display: block;font-size: 23px;font-weight: bold;color: #85ce36;">Mutations & Imprint</span><br>
            <br>
            <div class="row">
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="maternal_mutation" id="maternal_mutation" placeholder="Maternal Mutation" value="{{$dino_data->maternal_mut}}">
                </div>
                <div class="col-xs-6">
                  <input type="number" class="form-control underlined" name="paternal_mutation" id="paternal_mutation" placeholder="Paternal Mutation" value="{{$dino_data->paternal_mut}}">
                </div>
            </div>
            <br>
            <input type="number" class="form-control underlined" name="imprint" id="imprint" placeholder="Imprint Percentage" min="0" max="100" value="{{$dino_data->imprinting}}">
            <br>
            <center><button type="submit" class="btn btn-oval btn-primary">Save Changes</button></center>
          </form>
         </div>
        </div>
       </div>

       <div class="col col-xs-12 col-sm-12 col-md-3 col-xl-3 stats-col">

       </div>
      </div>
     </section>
     <section class="section map-tasks">
     </section>
    </article>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script>
  $('#torpor').change(function() {
    $('#base_torpor').val($(this).val());
  });

  $('#base_torpor').change(function() {
    $('#torpor').val($(this).val());
  });
  </script>
  <script>
  document.querySelector('#dinoPicture').addEventListener('change', function () {
    var filename = document.querySelector("#dinoPicture").value;
    if (/^\s*$/.test(filename)) {
      document.querySelector(".file-upload").classList.remove('active');
      document.querySelector("#noFile").innerText = "No file chosen...";
    }
    else {
      document.querySelector(".file-upload").classList.add('active');
      document.querySelector("#noFile").innerText = filename.replace("C:\\fakepath\\", "");
    }
  });
  </script>
  <script src="{{$server_url}}/js/vendor.js"></script>
  <script src="{{$server_url}}/js/app.js"></script>
 </body>
</html>

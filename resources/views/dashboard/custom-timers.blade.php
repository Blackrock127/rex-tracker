<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Rex Tracker</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{$server_url}}/pics/logo.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="{{$server_url}}/css/vendor.css">
  <!-- Theme initialization -->
  <link rel="stylesheet" id="theme-style" href="{{$server_url}}/css/app.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 </head>
 <body>
  <div class="main-wrapper">
   <div class="app" id="app">
    <header class="header">
     <div class="header-block header-block-collapse hidden-lg-up">
      <button class="collapse-btn" id="sidebar-collapse-btn"> <i class="fa fa-bars"></i> </button>
     </div>
     <div class="header-block header-block-nav">
      <ul class="nav-profile">
       <li class="notifications new"> <a href="" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <sup> <span class="counter"></span> </sup> </a>
        <div class="dropdown-menu notifications-dropdown-menu">
          <ul class="notifications-container">
            <center>
                        <br>
                        <i class="fa fa-bell-o fa-3x" aria-hidden="true"></i><br><br>
                        <p style="font-size:16px;">You do not have any notifications</p><br>
                        <br>
            </center>
          </ul>
         <!--<footer>
          <ul>
           <li> <a href=""> View All </a> </li>
          </ul>
        </footer>-->
        </div> </li>
        @if (Auth::guest())
        <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/default.png')">
          </div> <span> Guest </span> </a>
         <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
          <a class="dropdown-item" href="/login"> <i class="fa fa-sign-in icon"></i> Login </a>
          <a class="dropdown-item" href="/register"> <i class="fa fa-user icon"></i> Register </a>
         </div> </li>
         @else
          <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/{{ Auth::user()->picture }}')">
            </div> <span> {{ Auth::user()->name }} </span> </a>
           <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
            <a class="dropdown-item" href="/dashboard"> <i class="fa fa-tachometer icon"></i> Dashboard </a>
            <a class="dropdown-item" href="/dino-network"> <i class="fa fa-user icon"></i> Profile </a>
            <a class="dropdown-item" href="/settings"> <i class="fa fa-gear icon"></i> Settings </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/logout"> <i class="fa fa-power-off icon"></i> Logout </a>
           </div> </li>
          @endif
      </ul>
     </div>
    </header>
    <aside class="sidebar">
     <div class="sidebar-container">
      <div class="sidebar-header">
       <div class="brand" style="padding-top: 17px;line-height: 34px;">
          <img src="{{$server_url}}/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -20px;">
          <p style="font-size: 25px;display: inline;">TRACKER<p>
       </div>
      </div>
      <nav class="menu">
       <ul class="nav metismenu" id="sidebar-menu">
         <li> <a href="/dashboard"> <i class="fa fa-tachometer"></i> Dashboard </a> </li>
         <li> <a href="/tamed-dinos"> <i class="fa fa-list"></i> Tamed Dinos</a> </li>
         <li> <a href="/tribe-logs"> <i class="fa fa-history"></i> Tribe Logs</a> </li>
         <br>
         <li class="active open">
           <a href="">
             <i class="fa fa-clock-o"></i>Timers
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <li> <a href="/turret-timers"> <i class="fa fa-clock-o"></i> Turret Refill Timer</a> </li>
             <li class="active"> <a href="/custom-timers"> <i class="fa fa-clock-o"></i> Custom Timers</a> </li>
           </ul>
         </li>
         <li>
           <a href="">
             <i class="fa fa-calculator"></i>Calculators
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <!--<li> <a href="/taming-calculator"> <i class="fa fa-calculator"></i> Taming Calculator</a> </li>
             <li> <a href="/breeding-calculator"> <i class="fa fa-calculator"></i> Breeding Calculator</a> </li>
             <li> <a href="/stats-calculator"> <i class="fa fa-calculator"></i> Stats Calculator</a> </li>-->
             <li> <a href="/element-calculator"> <i class="fa fa-calculator"></i> Element Calculator</a> </li>
             <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Mortar Calculator</a> </li>
             <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Forge Calculator</a> </li>
           </ul>
         </li>
         <br>
         <li> <a href="/dino-network"> <i class="fa fa-globe"></i> Dino Network</a> </li>
         <li> <a href="/patch-notes"> <i class="fa fa-arrow-circle-down"></i> Patch Notes</a> </li>
        <!--<li> <a href=""> <i class="fa fa-th-large"></i> Items Manager <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="items-list.html"> Items List </a> </li>
          <li> <a href="item-editor.html"> Item Editor </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-bar-chart"></i> Charts <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="charts-flot.html"> Flot Charts </a> </li>
          <li> <a href="charts-morris.html"> Morris Charts </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-table"></i> Tables <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="static-tables.html"> Static Tables </a> </li>
          <li> <a href="responsive-tables.html"> Responsive Tables </a> </li>
         </ul> </li>
        <li> <a href="forms.html"> <i class="fa fa-pencil-square-o"></i> Forms </a> </li>
        <li> <a href=""> <i class="fa fa-desktop"></i> UI Elements <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="buttons.html"> Buttons </a> </li>
          <li> <a href="cards.html"> Cards </a> </li>
          <li> <a href="typography.html"> Typography </a> </li>
          <li> <a href="icons.html"> Icons </a> </li>
          <li> <a href="grid.html"> Grid </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-file-text-o"></i> Pages <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="login.html"> Login </a> </li>
          <li> <a href="signup.html"> Sign Up </a> </li>
          <li> <a href="reset.html"> Reset </a> </li>
          <li> <a href="error-404.html"> Error 404 App </a> </li>
          <li> <a href="error-404-alt.html"> Error 404 Global </a> </li>
          <li> <a href="error-500.html"> Error 500 App </a> </li>
          <li> <a href="error-500-alt.html"> Error 500 Global </a> </li>
         </ul> </li>
        <li> <a href="https://github.com/modularcode/modular-admin-html"> <i class="fa fa-github-alt"></i> Theme Docs </a> </li>-->
       </ul>
      </nav>
     </div>
     <footer class="sidebar-footer">
      <ul class="nav metismenu" id="customize-menu">
       <li>
        <ul>
         <li class="customize">
          <div class="customize-item">
           <div class="row customize-header">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-4">
             <label class="title">fixed</label>
            </div>
            <div class="col-xs-4">
             <label class="title">static</label>
            </div>
           </div>
           <div class="row hidden-md-down">
            <div class="col-xs-4">
             <label class="title">Sidebar:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value="sidebar-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Header:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value="header-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Footer:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value="footer-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value=""> <span></span> </label>
            </div>
           </div>
          </div>
          <div class="customize-item">
           <ul class="customize-colors">
            <li> <span class="color-item color-red" data-theme="red"></span> </li>
            <li> <span class="color-item color-orange" data-theme="orange"></span> </li>
            <li> <span class="color-item color-green active" data-theme=""></span> </li>
            <li> <span class="color-item color-seagreen" data-theme="seagreen"></span> </li>
            <li> <span class="color-item color-blue" data-theme="blue"></span> </li>
            <li> <span class="color-item color-purple" data-theme="purple"></span> </li>
           </ul>
          </div> </li>
        </ul> </li>
      </ul>
     </footer>
    </aside>
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <article class="content dashboard-page" >
     <section class="section">
      <div class="row">
       <div class="col col-xs-12 col-sm-12 col-md-1 col-xl-1 stats-col">

       </div>
       <div class="col col-xs-12 col-sm-12 col-md-10 col-xl-10 stats-col">
        <div class="card" data-exclude="xs">
          <div class="card-header bordered">
          <div class="header-block">
           <h3 class="title">Custom Timers - 0 Active</h3>
          </div>
         </div>
         <div class="card-block" style="min-height:400px;">
           <a href="#/tribe-manager/promote/" class="btn btn-primary btn-sm"><i class="fa fa-arrow-up" aria-hidden="true"></i>Create New Timer</a>
             <table class="table table-hover">
                                                             <thead>
                                                                 <tr>
                                                                     <th>#</th>
                                                                     <th>Timer Name</th>
                                                                     <th>Time Left</th>
                                                                     <th>Status</th>
                                                                     <th>Edit</th>
                                                                 </tr>
                                                             </thead>
                                                             <tbody>
                                                                 @foreach($timers as $timer)
                                                                 <tr>
                                                                     <th scope="row">1</th>
                                                                     <td>{{$timer->name}}</td>
                                                                     <td><div data-countdown="{{$timer->new_date}}"></div></td>
                                                                     <td>{{$timer->status}}</td>
                                                                     <td>
                                                                       <a class="btn btn-primary btn-sm"><i class="fa fa-arrow-up" aria-hidden="true"></i> Restart</a>
                                                                       <a class="btn btn-warning btn-sm"><i class="fa fa-arrow-down" aria-hidden="true"></i> Stop</a>
                                                                       <a class="btn btn-danger btn-sm"><i class="fa fa-ban" aria-hidden="true"></i> Delete</a>
                                                                     </td>
                                                                 </tr>
                                                                 @endforeach
                                                                 <!--<tr>
                                                                     <th scope="row">2</th>
                                                                     <td>Test Timer 1</td>
                                                                     <td><div id="test" data-countdown="2018/01/29"></div></td>
                                                                     <td>Stopped</td>
                                                                     <td>
                                                                       <a href="#/tribe-manager/promote/" class="btn btn-primary btn-sm"><i class="fa fa-arrow-up" aria-hidden="true"></i> Restart</a>
                                                                       <a href="#/tribe-manager/demote/" class="btn btn-warning btn-sm"><i class="fa fa-arrow-down" aria-hidden="true"></i> Stop</a>
                                                                       <a href="#/tribe-manager/kick/" class="btn btn-danger btn-sm"><i class="fa fa-ban" aria-hidden="true"></i> Delete</a>
                                                                     </td>
                                                                 </tr>-->
                                                             </tbody>
                                                         </table>
         </div>
        </div>
       </div>

       <div class="col col-xs-12 col-sm-12 col-md-1 col-xl-1 stats-col">

       </div>
      </div>
     </section>
     <section class="section map-tasks">
     </section>
    </article>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script src="{{$server_url}}/js/vendor.js"></script>
  <script src="{{$server_url}}/js/app.js"></script>
  <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
  <script src="https://cdn.rawgit.com/hilios/jQuery.countdown/2.2.0/dist/jquery.countdown.min.js"></script>
  <script>
  $('[data-countdown]').each(function() {
     var $this = $(this), finalDate = $(this).data('countdown');
     $this.countdown(finalDate, function(event) {
       $this.html(event.strftime('%D:%H:%M:%S'));
     });
  });

  $('div#test').countdown('stop');
  // Resume the countdown
  //$('div#clock').countdown('start');
  </script>
 </body>
</html>

<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Rex Tracker</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{$server_url}}/pics/logo.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="{{$server_url}}/css/vendor.css">
  <!-- Theme initialization -->
  <link rel="stylesheet" href="{{$server_url}}/css/app.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 </head>
 <body>
  <div class="main-wrapper">
   <div class="app" id="app">
    <header class="header">
     <div class="header-block header-block-collapse hidden-lg-up">
      <button class="collapse-btn" id="sidebar-collapse-btn"> <i class="fa fa-bars"></i> </button>
     </div>
     <div class="header-block header-block-nav">
      <ul class="nav-profile">
        <li class="notifications new"> <a href="" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <sup> <span class="counter"></span> </sup> </a>
         <div class="dropdown-menu notifications-dropdown-menu">
          <ul class="notifications-container">
            <center>
                        <br>
                        <i class="fa fa-bell-o fa-3x" aria-hidden="true"></i><br><br>
                        <p style="font-size:16px;">You do not have any notifications</p><br>
                        <br>
            </center>
          </ul>
          <!--<footer>
           <ul>
            <li> <a href=""> View All </a> </li>
           </ul>
         </footer>-->
         </div> </li>
         @if (Auth::guest())
         <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
           <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/default.png')">
           </div> <span> Guest </span> </a>
          <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
           <a class="dropdown-item" href="/login"> <i class="fa fa-sign-in icon"></i> Login </a>
           <a class="dropdown-item" href="/register"> <i class="fa fa-user icon"></i> Register </a>
          </div> </li>
          @else
           <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
             <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/{{ Auth::user()->picture }}')">
             </div> <span> {{ Auth::user()->name }} </span> </a>
            <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
             <a class="dropdown-item" href="/dashboard"> <i class="fa fa-tachometer icon"></i> Dashboard </a>
             <a class="dropdown-item" href="/dino-network"> <i class="fa fa-user icon"></i> Profile </a>
             <a class="dropdown-item" href="/settings"> <i class="fa fa-gear icon"></i> Settings </a>
             <div class="dropdown-divider"></div>
             <a class="dropdown-item" href="/logout"> <i class="fa fa-power-off icon"></i> Logout </a>
            </div> </li>
           @endif
      </ul>
     </div>
    </header>
    <aside class="sidebar">
     <div class="sidebar-container">
      <div class="sidebar-header">
       <div class="brand" style="padding-top: 17px;line-height: 34px;">
          <img src="{{$server_url}}/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -20px;">
          <p style="font-size: 25px;display: inline;">TRACKER<p>
       </div>
      </div>
      <nav class="menu">
       <ul class="nav metismenu" id="sidebar-menu">
         <li> <a href="/dashboard"> <i class="fa fa-tachometer"></i> Dashboard </a> </li>
         <li> <a href="/tamed-dinos"> <i class="fa fa-list"></i> Tamed Dinos</a> </li>
         <li> <a href="/tribe-logs"> <i class="fa fa-history"></i> Tribe Logs</a> </li>
         <br>
         <li>
           <a href="">
             <i class="fa fa-clock-o"></i>Timers
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <li> <a href="/turret-timers"> <i class="fa fa-clock-o"></i> Turret Refill Timer</a> </li>
             <li> <a href="/custom-timers"> <i class="fa fa-clock-o"></i> Custom Timers</a> </li>
           </ul>
         </li>
         <li class="active open">
           <a href="">
             <i class="fa fa-calculator"></i>Calculators
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav collapse in">
             <li> <a href="/element-calculator"> <i class="fa fa-calculator"></i> Element Calculator</a> </li>
             <li> <a href="/mortar-calculator"> <i class="fa fa-calculator"></i> Mortar Calculator</a> </li>
             <li class="active"> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Forge Calculator</a> </li>
           </ul>
         </li>
         <br>
         <li> <a href="/dino-network"> <i class="fa fa-globe"></i> Dino Network</a> </li>
         <li> <a href="/patch-notes"> <i class="fa fa-arrow-circle-down"></i> Patch Notes</a> </li>
        <!--<li> <a href=""> <i class="fa fa-th-large"></i> Items Manager <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="items-list.html"> Items List </a> </li>
          <li> <a href="item-editor.html"> Item Editor </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-bar-chart"></i> Charts <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="charts-flot.html"> Flot Charts </a> </li>
          <li> <a href="charts-morris.html"> Morris Charts </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-table"></i> Tables <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="static-tables.html"> Static Tables </a> </li>
          <li> <a href="responsive-tables.html"> Responsive Tables </a> </li>
         </ul> </li>
        <li> <a href="forms.html"> <i class="fa fa-pencil-square-o"></i> Forms </a> </li>
        <li> <a href=""> <i class="fa fa-desktop"></i> UI Elements <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="buttons.html"> Buttons </a> </li>
          <li> <a href="cards.html"> Cards </a> </li>
          <li> <a href="typography.html"> Typography </a> </li>
          <li> <a href="icons.html"> Icons </a> </li>
          <li> <a href="grid.html"> Grid </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-file-text-o"></i> Pages <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="login.html"> Login </a> </li>
          <li> <a href="signup.html"> Sign Up </a> </li>
          <li> <a href="reset.html"> Reset </a> </li>
          <li> <a href="error-404.html"> Error 404 App </a> </li>
          <li> <a href="error-404-alt.html"> Error 404 Global </a> </li>
          <li> <a href="error-500.html"> Error 500 App </a> </li>
          <li> <a href="error-500-alt.html"> Error 500 Global </a> </li>
         </ul> </li>
        <li> <a href="https://github.com/modularcode/modular-admin-html"> <i class="fa fa-github-alt"></i> Theme Docs </a> </li>-->
       </ul>
      </nav>
     </div>
     <footer class="sidebar-footer">
      <ul class="nav metismenu" id="customize-menu">
       <li>
        <ul>
         <li class="customize">
          <div class="customize-item">
           <div class="row customize-header">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-4">
             <label class="title">fixed</label>
            </div>
            <div class="col-xs-4">
             <label class="title">static</label>
            </div>
           </div>
           <div class="row hidden-md-down">
            <div class="col-xs-4">
             <label class="title">Sidebar:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value="sidebar-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Header:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value="header-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Footer:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value="footer-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value=""> <span></span> </label>
            </div>
           </div>
          </div>
          <div class="customize-item">
           <ul class="customize-colors">
            <li> <span class="color-item color-red" data-theme="red"></span> </li>
            <li> <span class="color-item color-orange" data-theme="orange"></span> </li>
            <li> <span class="color-item color-green active" data-theme=""></span> </li>
            <li> <span class="color-item color-seagreen" data-theme="seagreen"></span> </li>
            <li> <span class="color-item color-blue" data-theme="blue"></span> </li>
            <li> <span class="color-item color-purple" data-theme="purple"></span> </li>
           </ul>
          </div> </li>
        </ul> </li>
      </ul>
     </footer>
    </aside>
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <article class="content dashboard-page" >
     <section class="section">
      <div class="row">
       <div class="col col-xs-12 col-sm-12 col-md-3 col-xl-3 stats-col">

       </div>
       <div class="col col-xs-12 col-sm-12 col-md-6 col-xl-6 stats-col">
        <div class="card" data-exclude="xs">
         <div class="card-block" style="">
           <form action="/forge-calculator" method="POST">
             {{ csrf_field() }}
            <center><strong><p style="color:red;">{{$message}}</p></strong></center>
            <div class="form-group"> <label class="control-label">Metal Quantity</label>
            <input type="number" name="resources_count" name="resources_count" value="" class="form-control underlined" placeholder="Metal Quantity"><br>
            </div>
            <div class="form-group"> <label class="control-label">Forge Quantity</label>
            <input type="number" id="forge_count" name="forge_count" value="" class="form-control underlined" placeholder="Forge Quantity"><br>
            </div>
            <div class="form-group"> <label class="control-label">Fuel Type</label>
              <select class="form-control" id="fuel_type" name="fuel_type" style="border-radius: 0px;">
                                          <option disabled selected>Select Fuel Type</option>
                            							<option value="thatch">Thatch</option>
                            							<option value="wood">Wood</option>
                                          <option value="spark">Sparkpowder</option>
                            							<option value="angler">Angler Gel</option>
                                          <option value="gas">Gas</option>
              </select>
            </div>
              <center>
                    <button type="submit" class="btn btn-primary btn-submit" onclick="newPost()" style="margin-top: 8px;">
                        Calculate!
                    </button>
                  </center>
                  <br>
                  <hr>
            <p style="margin-top:10px;display:inline;">Refining Time: 0 hours and {{$calculate_time}} mins</p><a id="create" style="display:inline-block;margin-left:5px;" href="" class="btn btn-primary btn-sm"><i class="fa fa-clock-o" aria-hidden="true"></i> Create timer (<p id="time" style="display:inline-block;margin:0;"></p>)</a>
            <p><strong>Resources needed : </strong></p>
            <p>{{ $resource_quantity }} x <img alt="Blue Gem (Aberration).png" src="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/e/e1/Metal.png/128px-Metal.png?version=b46f045e7c847baf5636af8518224cec" width="25" height="25">Metal per forge</p>
            <p>{{ $calculate_resources }} x <img alt="Blue Gem (Aberration).png" src="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/d/df/Wood.png/128px-Wood.png?version=e8f6533b30b954f1ac12eef20499829a" width="25" height="25">Wood</p>
            <p>{{ $forge_quantity }} x <img alt="Blue Gem (Aberration).png" src="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/9/98/Refining_Forge.png/128px-Refining_Forge.png?version=912d5286d50be3f0ff2deb204d17ac08" width="25" height="25">Refining Forge</p>
            <p><strong>Output : </strong></p>
            <p>{{ $resource_output }} x <img alt="Blue Gem (Aberration).png" src="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/3/37/Metal_Ingot.png/20px-Metal_Ingot.png?version=cf2bd6dfa513fd5ed6d9f78b36a7e785" width="25" height="25">Metal Ingot</p>
          </form>
         </div>
        </div>
       </div>

       <div class="col col-xs-12 col-sm-12 col-md-3 col-xl-3 stats-col">

       </div>
      </div>
     </section>
     <section class="section map-tasks">
     </section>
    </article>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script src="{{$server_url}}/js/vendor.js"></script>
  <script src="{{$server_url}}/js/app.js"></script>
  <script>
  Number.prototype.toHHMMSS = function() {
    var hours = Math.floor(this / 3600) < 10 ? ("00" + Math.floor(this / 3600)).slice(-2) : Math.floor(this / 3600);
    var minutes = ("00" + Math.floor((this % 3600) / 60)).slice(-2);
    var seconds = ("00" + (this % 3600) % 60).slice(-2);
    return hours + ":" + minutes + ":" + seconds;
  }

  var time = (6 / 1) * 10 * {{$calculate_time}};
  document.getElementById("time").innerHTML = time.toHHMMSS();

  if(time < 1) {
    document.getElementById("create").hidden = true;
  } else {
    document.getElementById("create").hidden = false;
  }
  </script>
 </body>
</html>

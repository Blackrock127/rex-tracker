<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Rex Tracker</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{$server_url}}/pics/logo.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="{{$server_url}}/css/vendor.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{$server_url}}/css/app-{{$theme}}.css">
  <!-- Theme initialization -->
  <style>
  /* Pie Chart */
  .progress-pie-chart {
  width:200px;
  height: 200px;
  border-radius: 50%;
  background-color: #E5E5E5;
  position: relative;
  }
  .progress-pie-chart.gt-50 {
  background-color: #85ce36;
  }

  .ppc-progress {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 100px);
  top: calc(50% - 100px);
  width: 200px;
  height: 200px;
  clip: rect(0, 200px, 200px, 100px);
  }
  .ppc-progress .ppc-progress-fill {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 100px);
  top: calc(50% - 100px);
  width: 200px;
  height: 200px;
  clip: rect(0, 100px, 200px, 0);
  background: #85ce36;
  transform: rotate(0deg);
  }
  .gt-50 .ppc-progress {
  clip: rect(0, 100px, 200px, 0);
  }
  .gt-50 .ppc-progress .ppc-progress-fill {
  clip: rect(0, 200px, 200px, 100px);
  background: #E5E5E5;
  }

  .ppc-percents {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 173.91304px/2);
  top: calc(50% - 173.91304px/2);
  width: 173.91304px;
  height: 173.91304px;
  background: #fff;
  text-align: center;
  display: table;
  }
  .ppc-percents span {
  display: block;
  font-size: 2.6em;
  font-weight: bold;
  color: #85ce36;
  }

  .pcc-percents-wrapper {
  display: table-cell;
  vertical-align: middle;
  }

  .progress-pie-chart {
  margin: 50px auto 0;
  }
  </style>
 </head>
 <body>
  <div class="main-wrapper">
   <div class="app" id="app">
    <header class="header">
     <div class="header-block header-block-collapse hidden-lg-up">
      <button class="collapse-btn" id="sidebar-collapse-btn"> <i class="fa fa-bars"></i> </button>
     </div>
     <div class="header-block header-block-nav">
      <ul class="nav-profile">
       <li class="notifications new">
         <a href="" data-toggle="dropdown">
           @if($notifications_count == 0)
           <i class="fa fa-bell-o"></i> <sup> <span class="counter"></span> </sup>
           @else
          <i class="fa fa-bell-o"></i> <sup> <span class="counter">{{$notifications_count}}</span> </sup>
           @endif
         </a>
        <div class="dropdown-menu notifications-dropdown-menu">
         <ul class="notifications-container" id="notification">
          @if($notifications_count == 0)
          <center>
            <br>
            <i class="fa fa-bell-o fa-3x" aria-hidden="true"></i><br><br>
            <p style="font-size:16px;">You do not have any notifications</p><br>
            <br>
          </center>
          @else
          @endif
         </ul>
         @if($notifications_count > 0)
         <footer>
          <ul>
           <li> <a href=""> View All </a> </li>
          </ul>
        </footer>
        @else
        @endif
        </div>
      </li>
      @if (Auth::guest())
      <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/default.png')">
        </div> <span> Guest </span> </a>
       <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
        <a class="dropdown-item" href="/login"> <i class="fa fa-sign-in icon"></i> Login </a>
        <a class="dropdown-item" href="/register"> <i class="fa fa-user icon"></i> Register </a>
       </div> </li>
       @else
        <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/{{ Auth::user()->picture }}')">
          </div> <span> {{ Auth::user()->name }} </span> </a>
         <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
          <a class="dropdown-item" href="/dashboard"> <i class="fa fa-tachometer icon"></i> Dashboard </a>
          <a class="dropdown-item" href="/dino-network"> <i class="fa fa-user icon"></i> Profile </a>
          <a class="dropdown-item" href="/settings"> <i class="fa fa-gear icon"></i> Settings </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/logout"> <i class="fa fa-power-off icon"></i> Logout </a>
         </div> </li>
        @endif
      </ul>
     </div>
    </header>
    <aside class="sidebar">
     <div class="sidebar-container">
      <div class="sidebar-header">
       <div class="brand" style="padding-top: 17px;line-height: 34px;">
          <img src="{{$server_url}}/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -20px;">
          <p style="font-size: 25px;display: inline;">TRACKER<p>
       </div>
      </div>
      <nav class="menu">

       <ul class="nav metismenu" id="sidebar-menu">
        <li class="active"> <a href="/dashboard"> <i class="fa fa-tachometer"></i> Dashboard </a> </li>
        <li> <a href="/tamed-dinos"> <i class="fa fa-list"></i> Tamed Dinos</a> </li>
        <li> <a href="/tribe-logs"> <i class="fa fa-history"></i> Tribe Logs</a> </li>
        <br>
        <li>
          <a href="">
            <i class="fa fa-clock-o"></i>Timers
            <i class="fa arrow"></i>
          </a>
          <ul class="sidebar-nav">
            <li> <a href="/turret-timers"> <i class="fa fa-clock-o"></i> Turret Refill Timer</a> </li>
            <li> <a href="/custom-timers"> <i class="fa fa-clock-o"></i> Custom Timers</a> </li>
          </ul>
        </li>
        <li>
          <a href="">
            <i class="fa fa-calculator"></i>Calculators
            <i class="fa arrow"></i>
          </a>
          <ul class="sidebar-nav">
            <!--<li> <a href="/taming-calculator"> <i class="fa fa-calculator"></i> Taming Calculator</a> </li>
            <li> <a href="/breeding-calculator"> <i class="fa fa-calculator"></i> Breeding Calculator</a> </li>
            <li> <a href="/stats-calculator"> <i class="fa fa-calculator"></i> Stats Calculator</a> </li>-->
            <li> <a href="/element-calculator"> <i class="fa fa-calculator"></i> Element Calculator</a> </li>
            <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Mortar Calculator</a> </li>
            <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Forge Calculator</a> </li>
          </ul>
        </li>
        <br>
        <li> <a href="/dino-network"> <i class="fa fa-globe"></i> Dino Network</a> </li>
        <li> <a href="/patch-notes"> <i class="fa fa-arrow-circle-down"></i> Patch Notes</a> </li>
        <!--<li> <a href=""> <i class="fa fa-th-large"></i> Items Manager <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="items-list.html"> Items List </a> </li>
          <li> <a href="item-editor.html"> Item Editor </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-bar-chart"></i> Charts <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="charts-flot.html"> Flot Charts </a> </li>
          <li> <a href="charts-morris.html"> Morris Charts </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-table"></i> Tables <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="static-tables.html"> Static Tables </a> </li>
          <li> <a href="responsive-tables.html"> Responsive Tables </a> </li>
         </ul> </li>
        <li> <a href="forms.html"> <i class="fa fa-pencil-square-o"></i> Forms </a> </li>
        <li> <a href=""> <i class="fa fa-desktop"></i> UI Elements <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="buttons.html"> Buttons </a> </li>
          <li> <a href="cards.html"> Cards </a> </li>
          <li> <a href="typography.html"> Typography </a> </li>
          <li> <a href="icons.html"> Icons </a> </li>
          <li> <a href="grid.html"> Grid </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-file-text-o"></i> Pages <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="login.html"> Login </a> </li>
          <li> <a href="signup.html"> Sign Up </a> </li>
          <li> <a href="reset.html"> Reset </a> </li>
          <li> <a href="error-404.html"> Error 404 App </a> </li>
          <li> <a href="error-404-alt.html"> Error 404 Global </a> </li>
          <li> <a href="error-500.html"> Error 500 App </a> </li>
          <li> <a href="error-500-alt.html"> Error 500 Global </a> </li>
         </ul> </li>
        <li> <a href="https://github.com/modularcode/modular-admin-html"> <i class="fa fa-github-alt"></i> Theme Docs </a> </li>-->
       </ul>
      </nav>
     </div>
     <footer class="sidebar-footer">
      <ul class="nav metismenu" id="customize-menu">
       <li>
        <ul>
         <li class="customize">
          <div class="customize-item">
           <div class="row customize-header">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-4">
             <label class="title">fixed</label>
            </div>
            <div class="col-xs-4">
             <label class="title">static</label>
            </div>
           </div>
           <div class="row hidden-md-down">
            <div class="col-xs-4">
             <label class="title">Sidebar:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value="sidebar-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Header:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value="header-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Footer:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value="footer-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value=""> <span></span> </label>
            </div>
           </div>
          </div>
          <div class="customize-item">
           <ul class="customize-colors">
            <li> <span class="color-item color-red" data-theme="red"></span> </li>
            <li> <span class="color-item color-orange" data-theme="orange"></span> </li>
            <li> <span class="color-item color-green active" data-theme=""></span> </li>
            <li> <span class="color-item color-seagreen" data-theme="seagreen"></span> </li>
            <li> <span class="color-item color-blue" data-theme="blue"></span> </li>
            <li> <span class="color-item color-purple" data-theme="purple"></span> </li>
           </ul>
          </div> </li>
        </ul> </li>
      </ul>
     </footer>
    </aside>
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <article class="content dashboard-page">
     <section class="section">
      <div class="row sameheight-container">
       <div class="col col-xs-12 col-sm-12 col-md-6 col-xl-5 stats-col">
        <div class="card sameheight-item stats" data-exclude="xs">
          <div class="card-header">
           <div class="header-block">
            <h3 class="title" style="margin-top: 3px;">Survivor Stats</h3>
           </div>
          </div>
         <div class="card-block">
            <center><img src="https://serversftw.com/wp-content/uploads/2017/07/ark-survival-evolved-logo.png" alt="ARK Logo" width="152" height="152"></center>
            <center><span style="display: block;font-size: 23px;font-weight: bold;color: #85ce36;">Survivor Created on:</span></center>
            <center><h5 class="text">{{$survivor->server}}</h5></center>
            <br>
            <center><span style="display: block;font-size: 23px;font-weight: bold;color: #85ce36;">Current Server:</span></center>
            <center><h5>{{$survivor->server}}</h5></center>
            <br>
            <center><span style="display: block;font-size: 23px;font-weight: bold;color: #85ce36;">Survivors on this account :</span></center>
            <center><h5>1 Survivor</h5></center>
            <br>
            <center><span style="display: block;font-size: 23px;font-weight: bold;color: #85ce36;">Selected Survivor:</span></center>
            <select class="form-control" style="border-radius: 0px;">
              @foreach($survivors as $survivor)
							<option value="{{$survivor->id}}">{{$survivor->name}} - {{$survivor->server}}</option>
              @endforeach
						</select>
          </p>
         </div>
        </div>
       </div>
       <div class="col col-xs-12 col-sm-12 col-md-6 col-xl-7 history-col">
        <div class="card sameheight-item" data-exclude="xs">
         <div class="card-header">
          <div class="header-block">
           <h3 class="title" style="margin-top: 3px;">Survivor Stats</h3>
          </div>
         </div>
         <div class="card-block" style="margin-top: -18px;">
           <div class="bar_container">
           <div id="main_container">
           <div id="pbar" class="progress-pie-chart" data-percent="0">
           <div class="ppc-progress">
           <div class="ppc-progress-fill"></div>
           </div>
           <div class="ppc-percents">
           <div class="pcc-percents-wrapper">
           <h4>Level</h4>
           <span>0</span>
           </div>
           </div>
           </div>
           <progress style="display: none" id="progress_bar" value="0" max="1500"></progress>
           </div>
           </div>
                           <div class="row">
                             <center>
                             <div class="col-sm-6">
                               <br>
                               <ul class="item-list">
                                     <li><img src="{{$server_url}}/pics/Icons/80px-Health.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Health : 300.00</li>
                                     <li><img src="{{$server_url}}/pics/Icons/80px-Oxygen.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Oxygen : 100.00</li>
                                     <li><img src="{{$server_url}}/pics/Icons/80px-Water.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Water : 100.00</li>
                                     <li><img src="{{$server_url}}/pics/Icons/80px-Melee_Damage.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Melee Damage : 100.00</li>
                                     <li><img src="{{$server_url}}/pics/Icons/80px-Fortitude.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Fortitude : 30.00%</li>
                               </ul>
                             </div>
                             </center>
                             <center>
                             <div class="col-sm-6">
                               <br>
                               <ul class="item-list">
                                     <li><img src="{{$server_url}}/pics/Icons/80px-Stamina.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Stamina : 100.00</li>
                                     <li><img src="{{$server_url}}/pics/Icons/80px-Food.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Food : 100.00</li>
                                     <li><img src="{{$server_url}}/pics/Icons/80px-Weight.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Weight : 300.00</li>
                                     <li><img src="{{$server_url}}/pics/Icons/80px-Movement_Speed.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Movement Speed : 115.00%</li>
                                     <li><img src="{{$server_url}}/pics/Icons/80px-Crafting_Speed.png" style="width: 40px;height: 40px;margin-bottom: 5px;margin-right: 2px;"> Crafting Skill : 0.00%</li>
                               </ul>
                             </div>
                           </center></div>

                  </div>
        </div>
       </div>
      </div>
     </section>
     <section class="section map-tasks">
      <div class="row sameheight-container">
       <div class="col-md-8">
        <div class="card sameheight-item" data-exclude="xs,sm">
         <div class="card-header">
          <div class="header-block">
           @if($has_tribe == "true")
           <h3 class="title"> Tribe Manager - {{$tribe->name}}</h3>
           @else
           <h3 class="title"> Tribe Manager </h3>
           @endif
          </div>
          <div class="header-block pull-right">
           @if($theme == "Extinction")
           @else
           <a href="/search-users" class="btn btn-primary btn-sm rounded pull-right"> Invite user to tribe </a>
           @endif
          </div>
         </div>
         <div class="card-block">
           @if($has_tribe == "true")
           <!-- If has tribe -->
           @if($theme == "Extinction")
           <center>
             <br>
             <img src="https://vignette.wikia.nocookie.net/ark-survival-evolved/images/b/be/Obelisk.png/revision/latest?cb=20160125064046&path-prefix=ru" alt="Green Obelisk" width="152" height="219">
             <p style="font-size:16px;">Green Obelisk Connection Could Not Be Established!</p><br>
             <br>
           </center>
           @else
           <table class="table table-hover">
             <thead>
               <tr>
                 <th>Survivor Name</th>
                 <th>Rank</th>
                 <th>Edit</th>
               </tr>
            </thead>
            <tbody>
              @foreach($tribe_members as $tribe_member)
              <tr>
                <td>{{$tribe_member->name}}</td>
                <td>{{$tribe_member->role}}</td>
                <td>
                  @if($survivor_tribe_manager->role == "Owner")
                  <a href="#/tribe-manager/promote/{{$tribe_member->id}}" class="btn btn-primary btn-sm"><i class="fa fa-arrow-up" aria-hidden="true"></i> Promote</a>
                  <a href="#/tribe-manager/demote/{{$tribe_member->id}}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-down" aria-hidden="true"></i> Demote</a>
                  <a href="#/tribe-manager/kick/{{$tribe_member->id}}" class="btn btn-danger btn-sm"><i class="fa fa-ban" aria-hidden="true"></i> Kick</a>
                  @endif
                  @if($survivor_tribe_manager->role == "Admin")
                  <a href="#/tribe-manager/promote/{{$tribe_member->id}}" class="btn btn-primary btn-sm"><i class="fa fa-arrow-up" aria-hidden="true"></i> Promote</a>
                  <a href="#/tribe-manager/demote/{{$tribe_member->id}}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-down" aria-hidden="true"></i> Demote</a>
                  <a href="#/tribe-manager/kick/{{$tribe_member->id}}" class="btn btn-danger btn-sm"><i class="fa fa-ban" aria-hidden="true"></i> Kick</a>
                  @endif

                  @if($tribe_member->id == $survivor_tribe_manager->id)
                  <a href="#/tribe-manager/leave/{{$tribe_member->id}}" class="btn btn-danger btn-sm"><i class="fa fa-sign-out" aria-hidden="true"></i> Leave</a>
                  @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
          @endif
          <!-- If has tribe -->
          @else
          <center><p>You do not have a tribe!</p></center>
          <center><p>You can create a new tribe or join a tribe.</p></center>
          <center>
            <a href="" class="btn btn-primary btn-sm rounded" style="margin-right:10px;"> Create new tribe </a>
          </center>
          @endif
         </div>
        </div>
       </div>
       <div class="col-md-4">
        <div class="card tasks sameheight-item" data-exclude="xs,sm">
         <div class="card-header">
          <div class="header-block">
           <h3 class="title"> Tasks </h3>
          </div>
          <div class="header-block pull-right">
            @if($theme == "Extinction")
            @else
            <!--<a href="" class="btn btn-primary btn-sm rounded pull-right"> Create new task </a>-->
            @endif
          </div>
         </div>
         <div class="card-block">
          @if($has_tribe == "true")
          @if($theme == "Extinction")
          <center>
            <br>
            <img src="https://vignette.wikia.nocookie.net/ark-survival-evolved/images/b/be/Obelisk.png/revision/latest?cb=20160125064046&path-prefix=ru" alt="Green Obelisk" width="152" height="219">
            <p style="font-size:16px;">Green Obelisk Connection Could Not Be Established!</p><br>
          </center>
          @else
          <div class="tasks-block">
            <center><p>Tasks Not Available.</p></center>
           <!--<ul class="item-list">
            <li class="item">
             <div class="item-row">
              <div class="item-col item-col-title">
               <label> <input class="checkbox" type="checkbox"> <span>Do Metal Run</span> </label>
              </div>
              <div class="item-col fixed item-col-actions-dropdown">
               <div class="item-actions-dropdown">
                <a class="item-actions-toggle-btn"> <span class="inactive"> <i class="fa fa-cog"></i> </span> <span class="active"> <i class="fa fa-chevron-circle-right"></i> </span> </a>
                <div class="item-actions-block">
                 <ul class="item-actions-list">
                  <li> <a class="remove" href="#" data-toggle="modal" data-target="#confirm-modal"> <i class="fa fa-trash-o "></i> </a> </li>
                  <li> <a class="check" href="#"> <i class="fa fa-check"></i> </a> </li>
                  <li> <a class="edit" href="item-editor.html"> <i class="fa fa-pencil"></i> </a> </li>
                 </ul>
                </div>
               </div>
              </div>
             </div> </li>
             <li class="item">
              <div class="item-row">
               <div class="item-col item-col-title">
                <label> <input class="checkbox" type="checkbox"> <span>Make Kibble</span> </label>
               </div>
               <div class="item-col fixed item-col-actions-dropdown">
                <div class="item-actions-dropdown">
                 <a class="item-actions-toggle-btn"> <span class="inactive"> <i class="fa fa-cog"></i> </span> <span class="active"> <i class="fa fa-chevron-circle-right"></i> </span> </a>
                 <div class="item-actions-block">
                  <ul class="item-actions-list">
                   <li> <a class="remove" href="#" data-toggle="modal" data-target="#confirm-modal"> <i class="fa fa-trash-o "></i> </a> </li>
                   <li> <a class="check" href="#"> <i class="fa fa-check"></i> </a> </li>
                   <li> <a class="edit" href="item-editor.html"> <i class="fa fa-pencil"></i> </a> </li>
                  </ul>
                 </div>
                </div>
               </div>
              </div> </li>
              <li class="item">
               <div class="item-row">
                <div class="item-col item-col-title">
                 <label> <input class="checkbox" type="checkbox"> <span>Raid a tribe</span> </label>
                </div>
                <div class="item-col fixed item-col-actions-dropdown">
                 <div class="item-actions-dropdown">
                  <a class="item-actions-toggle-btn"> <span class="inactive"> <i class="fa fa-cog"></i> </span> <span class="active"> <i class="fa fa-chevron-circle-right"></i> </span> </a>
                  <div class="item-actions-block">
                   <ul class="item-actions-list">
                    <li> <a class="remove" href="#" data-toggle="modal" data-target="#confirm-modal"> <i class="fa fa-trash-o "></i> </a> </li>
                    <li> <a class="check" href="#"> <i class="fa fa-check"></i> </a> </li>
                    <li> <a class="edit" href="item-editor.html"> <i class="fa fa-pencil"></i> </a> </li>
                   </ul>
                  </div>
                 </div>
                </div>
               </div> </li>
               <li class="item">
                <div class="item-row">
                 <div class="item-col item-col-title">
                  <label> <input class="checkbox" type="checkbox"> <span>Make turrets</span> </label>
                 </div>
                 <div class="item-col fixed item-col-actions-dropdown">
                  <div class="item-actions-dropdown">
                   <a class="item-actions-toggle-btn"> <span class="inactive"> <i class="fa fa-cog"></i> </span> <span class="active"> <i class="fa fa-chevron-circle-right"></i> </span> </a>
                   <div class="item-actions-block">
                    <ul class="item-actions-list">
                     <li> <a class="remove" href="#" data-toggle="modal" data-target="#confirm-modal"> <i class="fa fa-trash-o "></i> </a> </li>
                     <li> <a class="check" href="#"> <i class="fa fa-check"></i> </a> </li>
                     <li> <a class="edit" href="item-editor.html"> <i class="fa fa-pencil"></i> </a> </li>
                    </ul>
                   </div>
                  </div>
                 </div>
                </div> </li>
                <li class="item">
                 <div class="item-row">
                  <div class="item-col item-col-title">
                   <label> <input class="checkbox" type="checkbox"> <span>Make a base on aberration</span> </label>
                  </div>
                  <div class="item-col fixed item-col-actions-dropdown">
                   <div class="item-actions-dropdown">
                    <a class="item-actions-toggle-btn"> <span class="inactive"> <i class="fa fa-cog"></i> </span> <span class="active"> <i class="fa fa-chevron-circle-right"></i> </span> </a>
                    <div class="item-actions-block">
                     <ul class="item-actions-list">
                      <li> <a class="remove" href="#" data-toggle="modal" data-target="#confirm-modal"> <i class="fa fa-trash-o "></i> </a> </li>
                      <li> <a class="check" href="#"> <i class="fa fa-check"></i> </a> </li>
                      <li> <a class="edit" href="item-editor.html"> <i class="fa fa-pencil"></i> </a> </li>
                     </ul>
                    </div>
                   </div>
                  </div>
                </div> </li>-->
           </ul>
         </div>
         @endif
          @else
          <center><p>You do not have a tribe!</p></center>
          <center><p>You can create a new tribe or join a tribe.</p></center>
          <center>
            <a href="" class="btn btn-primary btn-sm rounded" style="margin-right:10px;"> Create new tribe </a>
          </center>
          @endif
         </div>
        </div>
       </div>
      </div>
     </section>
    </article>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script>
  $(document).ready(function() {
  var progressbar = $('#progress_bar');
  max = progressbar.attr('max');
  time = (1000 / max) * 5;
  value = progressbar.val();
  text_value = 0;

  var loading = function() {
  if(value < {{$survivor->level}}) {
  text_value += 1;
  value += 1.0;
  addValue = progressbar.val(value);
  }

  $('.progress-value').html(+ '%');
  var $ppc = $('.progress-pie-chart'),
  deg = 360 * value / 100;
  if (value > 50) {
  $ppc.addClass('gt-50');
  }

  $('.ppc-progress-fill').css('transform', 'rotate(' + deg + 'deg)');
  $('.ppc-percents span').html(text_value);

  if (value == max) {
  clearInterval(animate);
  }
  };

  var animate = setInterval(function() {
  loading();
  }, time);
  });
  </script>
  <script >$('.ro-select').filter(function(){
  var $this = $(this),
      $sel = $('<ul>',{'class': 'ro-select-list'}),
      $wr = $('<div>', {'class': 'ro-select-wrapper'}),
      $inp = $('<input>', {
        type:'hidden',
        name: $this.attr('name'),
        'class': 'ro-select-input'
      }),
      $text = $('<div>', {
        'class':'ro-select-text ro-select-text-empty',
        text: $this.attr('placeholder')
      });
      $opts = $this.children('option');

  $text.click(function(){
    $sel.show();
  });

  $opts.filter(function(){
    var $opt = $(this);
    $sel.append($('<li>',{text:$opt.text(), 'class': 'ro-select-item'})).data('value',$opt.attr('value'));
  });
  $sel.on('click','li',function(){
    $text.text($(this).text()).removeClass('ro-select-text-empty');
    $(this).parent().hide().children('li').removeClass('ro-select-item-active');
    $(this).addClass('ro-select-item-active');
    $inp.val($this.data('value'));
  });
  $wr.append($text);
  $wr.append($('<i>', {'class':'fa fa-caret-down ro-select-caret'}));
  $this.after($wr.append($inp,$sel));
  $this.remove();
});
//# sourceURL=pen.js
</script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="{{$server_url}}/js/vendor.js"></script>
  <script src="{{$server_url}}/js/app.js"></script>
 </body>
</html>

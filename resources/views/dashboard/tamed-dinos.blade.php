<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title> ModularAdmin - Free Dashboard Theme | HTML Version </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="{{$server_url}}/pics/logo.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="{{$server_url}}/css/vendor.css">
  <!-- Theme initialization -->
  <link rel="stylesheet" href="{{$server_url}}/css/app-{{$theme}}.css">
 </head>
 <body>
  <div class="main-wrapper">
   <div class="app" id="app">
    <header class="header">
     <div class="header-block header-block-collapse hidden-lg-up">
      <button class="collapse-btn" id="sidebar-collapse-btn"> <i class="fa fa-bars"></i> </button>
     </div>
     <div class="header-block header-block-nav">
      <ul class="nav-profile">
       <li class="notifications new"> <a href="" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <sup> <span class="counter"></span> </sup> </a>
        <div class="dropdown-menu notifications-dropdown-menu">
          <ul class="notifications-container">
            <center>
                        <br>
                        <i class="fa fa-bell-o fa-3x" aria-hidden="true"></i><br><br>
                        <p style="font-size:16px;">You do not have any notifications</p><br>
                        <br>
            </center>
          </ul>
         <!--<footer>
          <ul>
           <li> <a href=""> View All </a> </li>
          </ul>
        </footer>-->
        </div> </li>
        @if (Auth::guest())
        <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/default.png')">
          </div> <span> Guest </span> </a>
         <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
          <a class="dropdown-item" href="/login"> <i class="fa fa-sign-in icon"></i> Login </a>
          <a class="dropdown-item" href="/register"> <i class="fa fa-user icon"></i> Register </a>
         </div> </li>
         @else
          <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/{{ Auth::user()->picture }}')">
            </div> <span> {{ Auth::user()->name }} </span> </a>
           <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
            <a class="dropdown-item" href="/dashboard"> <i class="fa fa-tachometer icon"></i> Dashboard </a>
            <a class="dropdown-item" href="/dino-network"> <i class="fa fa-user icon"></i> Profile </a>
            <a class="dropdown-item" href="/settings"> <i class="fa fa-gear icon"></i> Settings </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/logout"> <i class="fa fa-power-off icon"></i> Logout </a>
           </div> </li>
          @endif
      </ul>
     </div>
    </header>
    <aside class="sidebar">
     <div class="sidebar-container">
       <div class="sidebar-header">
        <div class="brand" style="padding-top: 17px;line-height: 34px;">
           <img src="/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -20px;">
           <p style="font-size: 25px;display: inline;">TRACKER<p>
        </div>
       </div>
      <nav class="menu">
       <ul class="nav metismenu" id="sidebar-menu">
         <li> <a href="/dashboard"> <i class="fa fa-tachometer"></i> Dashboard </a> </li>
         <li class="active"> <a href="/tamed-dinos"> <i class="fa fa-list"></i> Tamed Dinos</a> </li>
         <li> <a href="/tribe-logs"> <i class="fa fa-history"></i> Tribe Logs</a> </li>
         <br>
         <li>
           <a href="">
             <i class="fa fa-clock-o"></i>Timers
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <li> <a href="/turret-timers"> <i class="fa fa-clock-o"></i> Turret Refill Timer</a> </li>
             <li> <a href="/custom-timers"> <i class="fa fa-clock-o"></i> Custom Timers</a> </li>
           </ul>
         </li>
         <li>
           <a href="">
             <i class="fa fa-calculator"></i>Calculators
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <!--<li> <a href="/taming-calculator"> <i class="fa fa-calculator"></i> Taming Calculator</a> </li>
             <li> <a href="/breeding-calculator"> <i class="fa fa-calculator"></i> Breeding Calculator</a> </li>
             <li> <a href="/stats-calculator"> <i class="fa fa-calculator"></i> Stats Calculator</a> </li>-->
             <li> <a href="/element-calculator"> <i class="fa fa-calculator"></i> Element Calculator</a> </li>
             <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Mortar Calculator</a> </li>
             <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Forge Calculator</a> </li>
           </ul>
         </li>
         <br>
         <li> <a href="/dino-network"> <i class="fa fa-globe"></i> Dino Network</a> </li>
         <li> <a href="/patch-notes"> <i class="fa fa-arrow-circle-down"></i> Patch Notes</a> </li>
        <!--<li> <a href=""> <i class="fa fa-bar-chart"></i> Charts <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="charts-flot.html"> Flot Charts </a> </li>
          <li> <a href="charts-morris.html"> Morris Charts </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-table"></i> Tables <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="static-tables.html"> Static Tables </a> </li>
          <li> <a href="responsive-tables.html"> Responsive Tables </a> </li>
         </ul> </li>
        <li> <a href="forms.html"> <i class="fa fa-pencil-square-o"></i> Forms </a> </li>
        <li> <a href=""> <i class="fa fa-desktop"></i> UI Elements <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="buttons.html"> Buttons </a> </li>
          <li> <a href="cards.html"> Cards </a> </li>
          <li> <a href="typography.html"> Typography </a> </li>
          <li> <a href="icons.html"> Icons </a> </li>
          <li> <a href="grid.html"> Grid </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-file-text-o"></i> Pages <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="login.html"> Login </a> </li>
          <li> <a href="signup.html"> Sign Up </a> </li>
          <li> <a href="reset.html"> Reset </a> </li>
          <li> <a href="error-404.html"> Error 404 App </a> </li>
          <li> <a href="error-404-alt.html"> Error 404 Global </a> </li>
          <li> <a href="error-500.html"> Error 500 App </a> </li>
          <li> <a href="error-500-alt.html"> Error 500 Global </a> </li>
         </ul> </li>
        <li> <a href="https://github.com/modularcode/modular-admin-html"> <i class="fa fa-github-alt"></i> Theme Docs </a> </li>-->
       </ul>
      </nav>
     </div>
    </aside>
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <article class="content items-list-page" >
     <div class="title-search-block">
      <div class="title-block">
       <div class="row">
        <div class="col-md-6">
         <h3 class="title"> Tamed Dinos <a href="/tamed-dinos-new" class="btn btn-primary btn-sm rounded-s"> Create new dino </a></h3>
         <p class="title-description">Tribe Dino limit {{$dinos_count}}/{{$tribe_dino_limit->dino_limit}}</p>
        </div>
       </div>
      </div>
      <div class="items-search">
       <form class="form-inline">
        <div class="input-group">
         <input type="text" class="form-control boxed rounded-s" name="search" id="search" placeholder="Search...">
         <span class="input-group-btn"> <button class="btn btn-secondary rounded-s" type="button"> <i class="fa fa-search"></i> </button> </span>
        </div>
       </form>
      </div>
     </div>
     @if($has_tribe == "true")
     @if($theme == "Extinction")
     <center style="background-color:rgba(0,0,0,.3);">
       <br>
       <img src="https://vignette.wikia.nocookie.net/ark-survival-evolved/images/b/be/Obelisk.png/revision/latest?cb=20160125064046&path-prefix=ru" alt="Green Obelisk" width="300" height="453">
       <p style="font-size:16px;">Green Obelisk Connection Could Not Be Established!</p><br>
       <br>
     </center>
     @else
     <div class="card items">
      <ul class="item-list striped">
       <li class="item item-list-header hidden-sm-down">
        <div class="item-row">
         <div class="item-col item-col-header fixed item-col-img md">
          <div>
           <span></span>
          </div>
         </div>
         <div class="item-col item-col-header item-col-title">
          <div>
           <span style="margin-left: -71px;">Dino Name</span>
          </div>
         </div>
         <div class="item-col item-col-header item-col-sales">
          <div>
           <span>Dino Type</span>
          </div>
         </div>
         <div class="item-col item-col-header item-col-stats">
          <div class="no-overflow">
           <span>Gender</span>
          </div>
         </div>
         <div class="item-col item-col-header item-col-category">
          <div class="no-overflow">
           <span>Level</span>
          </div>
         </div>
         <div class="item-col item-col-header item-col-author">
          <div class="no-overflow">
           <span>Type</span>
          </div>
         </div>
         <div class="item-col item-col-header fixed item-col-actions-dropdown">
         </div>
        </div> </li>
       @foreach($dinos as $dino)
       <li class="item" onclick="location.href='/tamed-dinos/{{ $dino->id }}';">
        <div class="item-row">
         <div class="item-col fixed item-col-img md">
          <a>
           <div class="item-img rounded" style="background-image: url('{{$server_url}}/pics/src/tamed-dinos/{{$dino->dino_picture}}')"></div>
         </div>
         <div class="item-col fixed pull-left item-col-title">
          <div class="item-heading">
           Name
          </div>
          <div>
           <a href="/tamed-dinos/{{ $dino->id }}" style="text-decoration:none;">{{ $dino->name }}</a>
          </div>
         </div>
         <div class="item-col item-col-sales">
          <div class="item-heading">
           Dino Type
          </div>
          <div>
            {{$dino->type}}
          </div>
         </div>
         <div class="item-col item-col-stats no-overflow">
          <div class="item-heading">
           Gender
          </div>
          <div class="no-overflow">
           {{ $dino->gender }}
          </div>
         </div>
         <div class="item-col item-col-category no-overflow">
          <div class="item-heading">
            Level
          </div>
          <div class="no-overflow">
           Lvl {{ $dino->level }}
          </div>
         </div>
         <div class="item-col item-col-author">
          <div class="item-heading">
           Type
          </div>
          <div class="no-overflow">
           Normal
          </div>
         </div>
         <div class="item-col fixed item-col-actions-dropdown">
          <div class="item-actions-dropdown">
           <a class="edit" href="/tamed-dinos/{{$dino->id}}/edit"> <span class="inactive"> <i class="fa fa-pencil"></i> </span></a>
          </div>
         </div>
       </li>
       @endforeach
      </ul>
     </div>
     <!--<nav class="text-xs-right">
      <ul class="pagination">
       <li class="page-item"> <a class="page-link" href="" style="text-decoration:none;"> Prev </a> </li>
       <li class="page-item active"> <a class="page-link" href="" style="text-decoration:none;"> 1 </a> </li>
       <li class="page-item"> <a class="page-link" href="" style="text-decoration:none;"> 2 </a> </li>
       <li class="page-item"> <a class="page-link" href="" style="text-decoration:none;"> 3 </a> </li>
       <li class="page-item"> <a class="page-link" href="" style="text-decoration:none;"> 4 </a> </li>
       <li class="page-item"> <a class="page-link" href="" style="text-decoration:none;"> 5 </a> </li>
       <li class="page-item"> <a class="page-link" href="" style="text-decoration:none;"> Next </a> </li>
      </ul>
    </nav>-->
    </article>
    <div class="modal fade" id="confirm-modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header"> <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    					<span aria-hidden="true">×</span>
    				</button>
                                <h4 class="modal-title"><i class="fa fa-warning"></i> Alert</h4>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure want to do this?</p>
                            </div>
                            <div class="modal-footer"> <button type="button" class="btn btn-primary" data-dismiss="modal">Yes</button> <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button> </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
   </div>
  </div>
  @endif
  @else
  <center><p>You do not have a tribe!</p></center>
  <center><p>You can create a new tribe or join a tribe.</p></center>
  <center>
    <a href="" class="btn btn-primary btn-sm rounded" style="margin-right:10px;"> Create new tribe </a>
  </center>
  @endif
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script src="{{$server_url}}/js/vendor.js"></script>
  <script src="{{$server_url}}/js/app.js"></script>
 </body>
</html>

<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Rex Tracker</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="{{$server_url}}/css/vendor.css">
  <!-- Theme initialization -->
  <script>
            var themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) :
            {};
            var themeName = themeSettings.themeName || '';
            if (themeName)
            {
                document.write('<link rel="stylesheet" id="theme-style" href="{{$server_url}}/css/app-' + themeName + '.css">');
            }
            else
            {
                document.write('<link rel="stylesheet" id="theme-style" href="{{$server_url}}/css/app.css">');
            }
  </script>
 </head>
 <body>
  <div class="main-wrapper">
   <div class="app" id="app" style="background: #F7F7F7;background: url({{$server_url}}/pics/splash.jpg);background-size: cover;background-repeat: no-repeat;background-position: 50% 50%;padding-left:0px;">
    <header class="header" style="left: 0px;">
     <div class="header-block header-block-collapse hidden-lg-up">
      <button class="collapse-btn" id="sidebar-collapse-btn"> <i class="fa fa-bars"></i> </button>
     </div>
     <div class="header-block header-block-nav">
      <ul class="nav-profile">
       <li class="notifications new"> <a href="" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <sup> <span class="counter"></span> </sup> </a>
        <div class="dropdown-menu notifications-dropdown-menu">
         <ul class="notifications-container">
           <center>
                       <br>
                       <i class="fa fa-bell-o fa-3x" aria-hidden="true"></i><br><br>
                       <p style="font-size:16px;">You do not have any notifications</p><br>
                       <br>
           </center>
         </ul>
         <!--<footer>
          <ul>
           <li> <a href=""> View All </a> </li>
          </ul>
        </footer>-->
        </div> </li>
        @if (Auth::guest())
        <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/default.png')">
          </div> <span> Guest </span> </a>
         <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
          <a class="dropdown-item" href="/login"> <i class="fa fa-sign-in icon"></i> Login </a>
          <a class="dropdown-item" href="/register"> <i class="fa fa-user icon"></i> Register </a>
         </div> </li>
         @else
          <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <div class="img" style="background-image: url('{{$server_url}}/pics/src/users/{{ Auth::user()->picture }}')">
            </div> <span> {{ Auth::user()->name }} </span> </a>
           <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
            <a class="dropdown-item" href="/dashboard"> <i class="fa fa-tachometer icon"></i> Dashboard </a>
            <a class="dropdown-item" href="/dino-network"> <i class="fa fa-user icon"></i> Profile </a>
            <a class="dropdown-item" href="/settings"> <i class="fa fa-gear icon"></i> Settings </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/logout"> <i class="fa fa-power-off icon"></i> Logout </a>
           </div> </li>
          @endif
      </ul>
     </div>
    </header>
    <article class="content dashboard-page">
     <section class="section">
      <div class="row sameheight-container">
       <div class="col col-xs-12 col-sm-12 col-md-12 col-xl-12 stats-col">
        <div class="card sameheight-item stats" data-exclude="xs" style="background-color: transparent;">
         <div class="card-block">
          <div class="title-block">
           <h4 class="title"> </h4>
          </div>
                            <div class="" style="width: 100%;">
                                <center style="margin-left: 31%;margin-right: 31%;"><img src="{{$server_url}}/pics/logo.png" alt="Rex" style="width: 141px;height: 141px;margin-right: 2px;"><br><h1 style="font-size: 35px;color: #fff;text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;" id="text">TRACKER</h1><br><p style="font-size: 16px;color: #fff;margin-top: -30px;text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;">The Best and First ARK: Survival Evolved Tracker!</p></center>
                                <center style="margin-left: 0%;margin-right: 0%;"><h1 style="font-size: 35px;color: #fff;"><a href="/register" id="b2" class="btn btn-primary">Sign up!</a><a href="#learn-more" class="btn btn-primary" style="margin-left: 6px;">Learn more!</a></h1></center>
                            </div>
         </div>
        </div>
       </div>
      </div>
     </section>
    </article>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script src="{{$server_url}}/js/vendor.js"></script>
  <script src="{{$server_url}}/js/app.js"></script>
 </body>
</html>

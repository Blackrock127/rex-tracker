<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title> Rex Tracker </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="{{$server_url}}/css/vendor.css">
  <!-- Theme initialization -->
  <link rel="stylesheet" href="{{$server_url}}/css/app.css">
 </head>
 <body>
  <div class="auth" style="background-color: #f0f3f6;">
   <div class="auth-container">
    <div class="card">
     <header class="auth-header">
      <h1 class="auth-title">
        <img src="/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -16px;">
        TRACKER
      </h1>
     </header>
     <div class="auth-content" style="min-height: 0px;">
      <form id="login-form" action="/setup/tribe" method="POST" novalidate="">
        {{ csrf_field() }}
       <div class="form-group">
        <input type="text" class="form-control" id="tribe_name"  name="tribe_name" placeholder="Tribe Name">
       </div>
       <div class="form-group">
        <label for="remember"> <input class="checkbox" id="remember" name="remember" type="checkbox" onclick="toggleSelect()"> <span>Il join a tribe later</span> </label>
       </div>
       <div>
       <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary" id="button">Create Tribe</button>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script>
  window.onload = toggleSelect();

  function toggleSelect()
  {
    var isChecked = document.getElementById("remember").checked;
    document.getElementById("tribe_name").disabled = isChecked;
    if(isChecked) {
      document.getElementById("button").innerText = "Skip";
    } else {
      document.getElementById("button").innerText = "Create Tribe";
    }
  }
  </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <script src="{{$server_url}}/js/vendor.js"></script>
    <script src="{{$server_url}}/js/app.js"></script>
 </body>
</html>

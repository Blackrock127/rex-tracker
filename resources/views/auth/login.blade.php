@extends('layouts.app')

@section('content')
<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title> Rex Tracker </title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="css/vendor.css">
  <!-- Theme initialization -->
  <script>
            var themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) :
            {};
            var themeName = themeSettings.themeName || '';
            if (themeName)
            {
                document.write('<link rel="stylesheet" id="theme-style" href="css/app-' + themeName + '.css">');
            }
            else
            {
                document.write('<link rel="stylesheet" id="theme-style" href="css/app.css">');
            }
        </script>
 </head>
 <body>
  <div class="auth" style="background-color: #f0f3f6;">
   <div class="auth-container">
    <div class="card">
     <header class="auth-header">
      <h1 class="auth-title">
        <img src="/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -16px;">
        TRACKER
      </h1>
     </header>
     <div class="auth-content">
      <form id="login-form" action="/login" method="POST" novalidate="">
        {{ csrf_field() }}
       <div class="form-group">
        <input id="email" type="email" name="email" class="form-control underlined" name="username" id="username" placeholder="E-mail Address" required>
       </div>
       <br>
       <div class="form-group">
        <input id="password" type="password" name="password" class="form-control underlined" placeholder="Password" required>
       </div>
       <div class="form-group">
        <label for="remember"> <input class="checkbox" id="remember" type="checkbox"> <span>Remember me</span> </label>
        <a href="reset.html" class="forgot-btn pull-right" style="text-decoration: none;">Forgot password?</a>
       </div>
       <div class="form-group">
        <button type="submit" class="btn btn-block btn-primary">Login</button>
       </div>
       <div class="form-group">
        <p class="text-muted text-xs-center">Do not have an account? <a href="/register"  style="text-decoration: none;">Sign Up!</a></p>
       </div>
      </form>
     </div>
    </div>
   </div>
  </div>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script src="js/vendor.js"></script>
  <script src="js/app.js"></script>
 </body>
</html>
@endsection

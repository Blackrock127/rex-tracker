<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Rex Tracker</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="http://localhost/css/vendor.css">
  <!-- Theme initialization -->
  <link rel="stylesheet" href="http://localhost/css/app.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 </head>
 <body>
  <div class="main-wrapper">
   <div class="app" id="app">
    <header class="header">
     <div class="header-block header-block-collapse hidden-lg-up">
      <button class="collapse-btn" id="sidebar-collapse-btn"> <i class="fa fa-bars"></i> </button>
     </div>
     <div class="header-block header-block-nav">
      <ul class="nav-profile">
       <li class="notifications new"> <a href="" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <sup> <span class="counter"></span> </sup> </a>
        <div class="dropdown-menu notifications-dropdown-menu">
         <ul class="notifications-container">

         </ul>
         <footer>
          <ul>
           <li> <a href=""> View All </a> </li>
          </ul>
         </footer>
        </div> </li>
        @if (Auth::guest())
        <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
         <div class="img" style="background-image: url('https://secure.gravatar.com/avatar/0d5c7e85c4799c56b52fb5c650caf9b2?d=https://survivetheark.com/uploads/set_resources_33/84c1e40ea0e759e3f1505eb1788ddf3c_default_photo.png')">
         </div> <span class="name"> Guest </span> </a>
        <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
         <a class="dropdown-item" href="/login"> <i class="fa fa-sign-in pull-right"></i> Login </a>
         <a class="dropdown-item" href="/register"> <i class="fa fa-user pull-right"></i> Register </a>
        </div> </li>
        @else
        <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <div class="img" style="background-image: url('/pics/logo.png')">
          </div> <span class="name"> {{ Auth::user()->name }} </span> </a>
         <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
          <a class="dropdown-item" href="/dashboard"> <i class="fa fa-tachometer icon"></i> Dashboard </a>
          <a class="dropdown-item" href="/profile/{{ Auth::user()->id }}"> <i class="fa fa-user icon"></i> Profile </a>
          <a class="dropdown-item" href="/settings"> <i class="fa fa-gear icon"></i> Settings </a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/logout"> <i class="fa fa-power-off icon"></i> Logout </a>
         </div> </li>
        @endif
      </ul>
     </div>
    </header>
    <aside class="sidebar">
     <div class="sidebar-container">
      <div class="sidebar-header">
       <div class="brand" style="padding-top: 17px;line-height: 34px;">
          <img src="/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -20px;">
          <p style="font-size: 25px;display: inline;">TRACKER<p>
       </div>
      </div>
      <nav class="menu">
       <ul class="nav metismenu" id="sidebar-menu">
         <li> <a href="/dashboard"> <i class="fa fa-tachometer"></i> Dashboard </a> </li>
         <li> <a href="/tamed-dinos"> <i class="fa fa-list"></i> Tamed Dinos</a> </li>
         <li> <a href="/tribe-logs"> <i class="fa fa-history"></i> Tribe Logs</a> </li>
         <br>
         <li>
           <a href="">
             <i class="fa fa-clock-o"></i>Timers
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <li> <a href="/turret-refill-timer"> <i class="fa fa-clock-o"></i> Turret Refill Timer</a> </li>
             <li> <a href="/surface-timer"> <i class="fa fa-clock-o"></i> Surface Timer</a> </li>
           </ul>
         </li>
         <li>
           <a href="">
             <i class="fa fa-calculator"></i>Calculators
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav">
             <li> <a href="/taming-calculator"> <i class="fa fa-calculator"></i> Taming Calculator</a> </li>
             <li> <a href="/breeding-calculator"> <i class="fa fa-calculator"></i> Breeding Calculator</a> </li>
             <li> <a href="/stats-calculator"> <i class="fa fa-calculator"></i> Stats Calculator</a> </li>
             <li> <a href="/element-calculator"> <i class="fa fa-calculator"></i> Element Calculator</a> </li>
             <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Forge Calculator</a> </li>
           </ul>
         </li>
         <br>
         <li class="active open">
           <a href="" style="color: #fe974b;">
             <i class="fa fa-tachometer"></i>Admin Dashboard
             <i class="fa arrow"></i>
           </a>
           <ul class="sidebar-nav collapse in">
             <li> <a href="/admin-dashboard"> <i class="fa fa-tachometer"></i> Admin Dashboard</a> </li>
             <li> <a href="/admin-dashboard/users"> <i class="fa fa-user"></i> Manage Users</a> </li>
             <li class="active"> <a href="/admin-dashboard/tribes"> <i class="fa fa-list"></i> Manage Tribes</a> </li>
             <li> <a href="/admin-dashboard/management"> <i class="fa fa-wrench"></i> Management Tools</a> </li>
           </ul>
         </li>
         <br>
         <li> <a href="/dino-network"> <i class="fa fa-globe"></i> Dino Network</a> </li>
         <li> <a href="/patch-notes"> <i class="fa fa-arrow-circle-down"></i> Patch Notes</a> </li>
        <!--<li> <a href=""> <i class="fa fa-th-large"></i> Items Manager <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="items-list.html"> Items List </a> </li>
          <li> <a href="item-editor.html"> Item Editor </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-bar-chart"></i> Charts <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="charts-flot.html"> Flot Charts </a> </li>
          <li> <a href="charts-morris.html"> Morris Charts </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-table"></i> Tables <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="static-tables.html"> Static Tables </a> </li>
          <li> <a href="responsive-tables.html"> Responsive Tables </a> </li>
         </ul> </li>
        <li> <a href="forms.html"> <i class="fa fa-pencil-square-o"></i> Forms </a> </li>
        <li> <a href=""> <i class="fa fa-desktop"></i> UI Elements <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="buttons.html"> Buttons </a> </li>
          <li> <a href="cards.html"> Cards </a> </li>
          <li> <a href="typography.html"> Typography </a> </li>
          <li> <a href="icons.html"> Icons </a> </li>
          <li> <a href="grid.html"> Grid </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-file-text-o"></i> Pages <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="login.html"> Login </a> </li>
          <li> <a href="signup.html"> Sign Up </a> </li>
          <li> <a href="reset.html"> Reset </a> </li>
          <li> <a href="error-404.html"> Error 404 App </a> </li>
          <li> <a href="error-404-alt.html"> Error 404 Global </a> </li>
          <li> <a href="error-500.html"> Error 500 App </a> </li>
          <li> <a href="error-500-alt.html"> Error 500 Global </a> </li>
         </ul> </li>
        <li> <a href="https://github.com/modularcode/modular-admin-html"> <i class="fa fa-github-alt"></i> Theme Docs </a> </li>-->
       </ul>
      </nav>
     </div>
     <footer class="sidebar-footer">
      <ul class="nav metismenu" id="customize-menu">
       <li>
        <ul>
         <li class="customize">
          <div class="customize-item">
           <div class="row customize-header">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-4">
             <label class="title">fixed</label>
            </div>
            <div class="col-xs-4">
             <label class="title">static</label>
            </div>
           </div>
           <div class="row hidden-md-down">
            <div class="col-xs-4">
             <label class="title">Sidebar:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value="sidebar-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Header:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value="header-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Footer:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value="footer-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value=""> <span></span> </label>
            </div>
           </div>
          </div>
          <div class="customize-item">
           <ul class="customize-colors">
            <li> <span class="color-item color-red" data-theme="red"></span> </li>
            <li> <span class="color-item color-orange" data-theme="orange"></span> </li>
            <li> <span class="color-item color-green active" data-theme=""></span> </li>
            <li> <span class="color-item color-seagreen" data-theme="seagreen"></span> </li>
            <li> <span class="color-item color-blue" data-theme="blue"></span> </li>
            <li> <span class="color-item color-purple" data-theme="purple"></span> </li>
           </ul>
          </div> </li>
        </ul> </li>
      </ul>
     </footer>
    </aside>
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <article class="content dashboard-page" >
     <section class="section">
      <div class="row">
       <div class="col col-xs-12 col-sm-12 col-md-3 col-xl-3 stats-col">

       </div>
       <div class="col col-xs-12 col-sm-12 col-md-6 col-xl-6 stats-col">
        <div class="card" data-exclude="xs">
         <div class="card-block" style="">
            <input type="number" class="form-control underlined" placeholder="How many Element do you want to make?">
            <div class="row">
                <div class="col-xs-8">
                  <p style="margin-top:10px;">Crafting Time: 0 Hours</p>
                </div>
                <div class="col-xs-4">
                    <button type="button" class="btn btn-primary btn-submit pull-right" onclick="newPost()" style="margin-top: 8px;">
                        Calculate!
                    </button>
                </div>
            </div>
            <hr>
            <p>Resources needed : </p>
            <p>0 x <img alt="Blue Gem (Aberration).png" src="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/1/13/Blue_Gem_%28Aberration%29.png/25px-Blue_Gem_%28Aberration%29.png?version=b1f295ba818d960d43439ba6540e4baa" width="25" height="25" srcset="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/1/13/Blue_Gem_%28Aberration%29.png/38px-Blue_Gem_%28Aberration%29.png?version=b1f295ba818d960d43439ba6540e4baa 1.5x, https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/1/13/Blue_Gem_%28Aberration%29.png/50px-Blue_Gem_%28Aberration%29.png?version=b1f295ba818d960d43439ba6540e4baa 2x">Blue Gems</p>
            <p>0 x <img alt="Charge Battery (Aberration).png" src="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/0/00/Charge_Battery_%28Aberration%29.png/25px-Charge_Battery_%28Aberration%29.png?version=87e5c6d798e99f1876fb64684b3f7722" width="25" height="25" srcset="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/0/00/Charge_Battery_%28Aberration%29.png/38px-Charge_Battery_%28Aberration%29.png?version=87e5c6d798e99f1876fb64684b3f7722 1.5x, https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/0/00/Charge_Battery_%28Aberration%29.png/50px-Charge_Battery_%28Aberration%29.png?version=87e5c6d798e99f1876fb64684b3f7722 2x">Charge Battery</p>
            <p>0 x <img alt="Congealed Gas Ball (Aberration).png" src="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/7/7e/Congealed_Gas_Ball_%28Aberration%29.png/25px-Congealed_Gas_Ball_%28Aberration%29.png?version=d7a2514032b507eab8e7a6e743fc5f17" width="25" height="25" srcset="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/7/7e/Congealed_Gas_Ball_%28Aberration%29.png/38px-Congealed_Gas_Ball_%28Aberration%29.png?version=d7a2514032b507eab8e7a6e743fc5f17 1.5x, https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/7/7e/Congealed_Gas_Ball_%28Aberration%29.png/50px-Congealed_Gas_Ball_%28Aberration%29.png?version=d7a2514032b507eab8e7a6e743fc5f17 2x">Congealed Gas Ball</p>
            <p>0 x <img alt="Element Ore (Aberration).png" src="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/6/65/Element_Ore_%28Aberration%29.png/25px-Element_Ore_%28Aberration%29.png?version=b9330882f20e11d07dc098df535f0cbb" width="25" height="25" srcset="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/6/65/Element_Ore_%28Aberration%29.png/38px-Element_Ore_%28Aberration%29.png?version=b9330882f20e11d07dc098df535f0cbb 1.5x, https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/6/65/Element_Ore_%28Aberration%29.png/50px-Element_Ore_%28Aberration%29.png?version=b9330882f20e11d07dc098df535f0cbb 2x">Element Ore</p>
            <p>0 x <img alt="Green Gem (Aberration).png" src="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/c/c3/Green_Gem_%28Aberration%29.png/25px-Green_Gem_%28Aberration%29.png?version=ddd3c658a8937a7569852b97b99f842d" width="25" height="25" srcset="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/c/c3/Green_Gem_%28Aberration%29.png/38px-Green_Gem_%28Aberration%29.png?version=ddd3c658a8937a7569852b97b99f842d 1.5x, https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/c/c3/Green_Gem_%28Aberration%29.png/50px-Green_Gem_%28Aberration%29.png?version=ddd3c658a8937a7569852b97b99f842d 2x">Green Gems</p>
            <p>0 x <img alt="Red Gem (Aberration).png" src="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/a/a6/Red_Gem_%28Aberration%29.png/25px-Red_Gem_%28Aberration%29.png?version=e90fad706028ae17f6c6540fb3fc0ca9" width="25" height="25" srcset="https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/a/a6/Red_Gem_%28Aberration%29.png/38px-Red_Gem_%28Aberration%29.png?version=e90fad706028ae17f6c6540fb3fc0ca9 1.5x, https://d1u5p3l4wpay3k.cloudfront.net/arksurvivalevolved_gamepedia/thumb/a/a6/Red_Gem_%28Aberration%29.png/50px-Red_Gem_%28Aberration%29.png?version=e90fad706028ae17f6c6540fb3fc0ca9 2x"> Red Gems</p>
         </div>
        </div>
       </div>

       <div class="col col-xs-12 col-sm-12 col-md-3 col-xl-3 stats-col">

       </div>
      </div>
     </section>
     <section class="section map-tasks">
     </section>
    </article>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script src="http://localhost/js/vendor.js"></script>
  <script src="http://localhost/js/app.js"></script>
 </body>
</html>

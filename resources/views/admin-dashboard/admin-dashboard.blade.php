<!doctype html>
<html class="no-js" lang="en">
 <head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Rex Tracker</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="apple-touch-icon" href="apple-touch-icon.png">
  <!-- Place favicon.ico in the root directory -->
  <link rel="stylesheet" href="css/vendor.css">
  <!-- Theme initialization -->
  <style>
  /* Pie Chart */
  .progress-pie-chart {
  width:200px;
  height: 200px;
  border-radius: 50%;
  background-color: #E5E5E5;
  position: relative;
  }
  .progress-pie-chart.gt-50 {
  background-color: #85ce36;
  }

  .ppc-progress {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 100px);
  top: calc(50% - 100px);
  width: 200px;
  height: 200px;
  clip: rect(0, 200px, 200px, 100px);
  }
  .ppc-progress .ppc-progress-fill {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 100px);
  top: calc(50% - 100px);
  width: 200px;
  height: 200px;
  clip: rect(0, 100px, 200px, 0);
  background: #85ce36;
  transform: rotate(0deg);
  }
  .gt-50 .ppc-progress {
  clip: rect(0, 100px, 200px, 0);
  }
  .gt-50 .ppc-progress .ppc-progress-fill {
  clip: rect(0, 200px, 200px, 100px);
  background: #E5E5E5;
  }

  .ppc-percents {
  content: "";
  position: absolute;
  border-radius: 50%;
  left: calc(50% - 173.91304px/2);
  top: calc(50% - 173.91304px/2);
  width: 173.91304px;
  height: 173.91304px;
  background: #fff;
  text-align: center;
  display: table;
  }
  .ppc-percents span {
  display: block;
  font-size: 2.6em;
  font-weight: bold;
  color: #85ce36;
  }

  .pcc-percents-wrapper {
  display: table-cell;
  vertical-align: middle;
  }

  .progress-pie-chart {
  margin: 50px auto 0;
  }
  </style>
  <script>
            var themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) :
            {};
            var themeName = themeSettings.themeName || '';
            if (themeName)
            {
                document.write('<link rel="stylesheet" id="theme-style" href="css/app-' + themeName + '.css">');
            }
            else
            {
                document.write('<link rel="stylesheet" id="theme-style" href="css/app.css">');
            }
  </script>
 </head>
 <body>
  <div class="main-wrapper">
   <div class="app" id="app">
    <header class="header">
     <div class="header-block header-block-collapse hidden-lg-up">
      <button class="collapse-btn" id="sidebar-collapse-btn"> <i class="fa fa-bars"></i> </button>
     </div>
     <div class="header-block header-block-nav">
      <ul class="nav-profile">
       <li class="notifications new"> <a href="" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <sup> <span class="counter">8</span> </sup> </a>
        <div class="dropdown-menu notifications-dropdown-menu">
         <ul class="notifications-container">
          <li> <a href="" class="notification-item">
            <div class="img-col">
             <div class="img" style="background-image: url('assets/faces/3.jpg')"></div>
            </div>
            <div class="body-col">
             <p> <span class="accent">Zack Alien</span> pushed new commit: <span class="accent">Fix page load performance issue</span>. </p>
            </div> </a> </li>
          <li> <a href="" class="notification-item">
            <div class="img-col">
             <div class="img" style="background-image: url('assets/faces/5.jpg')"></div>
            </div>
            <div class="body-col">
             <p> <span class="accent">Amaya Hatsumi</span> started new task: <span class="accent">Dashboard UI design.</span>. </p>
            </div> </a> </li>
          <li> <a href="" class="notification-item">
            <div class="img-col">
             <div class="img" style="background-image: url('assets/faces/8.jpg')"></div>
            </div>
            <div class="body-col">
             <p> <span class="accent">Andy Nouman</span> deployed new version of <span class="accent">NodeJS REST Api V3</span> </p>
            </div> </a> </li>
         </ul>
         <footer>
          <ul>
           <li> <a href=""> View All </a> </li>
          </ul>
         </footer>
        </div> </li>
        @if (Auth::guest())
        <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
          <div class="img" style="background-image: url('https://secure.gravatar.com/avatar/0d5c7e85c4799c56b52fb5c650caf9b2?d=https://survivetheark.com/uploads/set_resources_33/84c1e40ea0e759e3f1505eb1788ddf3c_default_photo.png')">
          </div> <span> Guest </span> </a>
         <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
          <a class="dropdown-item" href="/login"> <i class="fa fa-sign-in icon"></i> Login </a>
          <a class="dropdown-item" href="/register"> <i class="fa fa-user icon"></i> Register </a>
         </div> </li>
         @else
          <li class="profile dropdown"> <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <div class="img" style="background-image: url('/pics/logo.png')">
            </div> <span> {{ Auth::user()->name }} </span> </a>
           <div class="dropdown-menu profile-dropdown-menu" aria-labelledby="dropdownMenu1">
            <a class="dropdown-item" href="/dashboard"> <i class="fa fa-tachometer icon"></i> Dashboard </a>
            <a class="dropdown-item" href="/profile/{{ Auth::user()->id }}"> <i class="fa fa-user icon"></i> Profile </a>
            <a class="dropdown-item" href="/settings"> <i class="fa fa-gear icon"></i> Settings </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/logout"> <i class="fa fa-power-off icon"></i> Logout </a>
           </div> </li>
          @endif
      </ul>
     </div>
    </header>
    <aside class="sidebar">
     <div class="sidebar-container">
      <div class="sidebar-header">
       <div class="brand" style="padding-top: 17px;line-height: 34px;">
          <img src="/pics/logo.png" alt="Rex" style="width:41px;height:41px;margin-top: -20px;">
          <p style="font-size: 25px;display: inline;">TRACKER<p>
       </div>
      </div>
      <nav class="menu">

       <ul class="nav metismenu" id="sidebar-menu">
        <li> <a href="/dashboard"> <i class="fa fa-tachometer"></i> Dashboard </a> </li>
        <li> <a href="/tamed-dinos"> <i class="fa fa-list"></i> Tamed Dinos</a> </li>
        <li> <a href="/tribe-logs"> <i class="fa fa-history"></i> Tribe Logs</a> </li>
        <br>
        <li>
          <a href="">
            <i class="fa fa-clock-o"></i>Timers
            <i class="fa arrow"></i>
          </a>
          <ul class="sidebar-nav">
            <li> <a href="/turret-refill-timer"> <i class="fa fa-clock-o"></i> Turret Refill Timer</a> </li>
            <li> <a href="/surface-timer"> <i class="fa fa-clock-o"></i> Surface Timer</a> </li>
          </ul>
        </li>
        <li>
          <a href="">
            <i class="fa fa-calculator"></i>Calculators
            <i class="fa arrow"></i>
          </a>
          <ul class="sidebar-nav">
            <li> <a href="/taming-calculator"> <i class="fa fa-calculator"></i> Taming Calculator</a> </li>
            <li> <a href="/breeding-calculator"> <i class="fa fa-calculator"></i> Breeding Calculator</a> </li>
            <li> <a href="/stats-calculator"> <i class="fa fa-calculator"></i> Stats Calculator</a> </li>
            <li> <a href="/element-calculator"> <i class="fa fa-calculator"></i> Element Calculator</a> </li>
            <li> <a href="/forge-calculator"> <i class="fa fa-calculator"></i> Forge Calculator</a> </li>
          </ul>
        </li>
        <br>
        <li class="active open">
          <a href="" style="color: #fe974b;">
            <i class="fa fa-tachometer"></i>Admin Dashboard
            <i class="fa arrow"></i>
          </a>
          <ul class="sidebar-nav collapse in">
            <li class="active"> <a href="/admin-dashboard"> <i class="fa fa-tachometer"></i> Admin Dashboard</a> </li>
            <li> <a href="/admin-dashboard/users"> <i class="fa fa-user"></i> Manage Users</a> </li>
            <li> <a href="/admin-dashboard/tribes"> <i class="fa fa-list"></i> Manage Tribes</a> </li>
            <li> <a href="/admin-dashboard/management"> <i class="fa fa-wrench"></i> Management Tools</a> </li>
          </ul>
        </li>
        <br>
        <li> <a href="/dino-network"> <i class="fa fa-globe"></i> Dino Network</a> </li>
        <li> <a href="/patch-notes"> <i class="fa fa-arrow-circle-down"></i> Patch Notes</a> </li>
        <!--<li> <a href=""> <i class="fa fa-th-large"></i> Items Manager <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="items-list.html"> Items List </a> </li>
          <li> <a href="item-editor.html"> Item Editor </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-bar-chart"></i> Charts <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="charts-flot.html"> Flot Charts </a> </li>
          <li> <a href="charts-morris.html"> Morris Charts </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-table"></i> Tables <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="static-tables.html"> Static Tables </a> </li>
          <li> <a href="responsive-tables.html"> Responsive Tables </a> </li>
         </ul> </li>
        <li> <a href="forms.html"> <i class="fa fa-pencil-square-o"></i> Forms </a> </li>
        <li> <a href=""> <i class="fa fa-desktop"></i> UI Elements <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="buttons.html"> Buttons </a> </li>
          <li> <a href="cards.html"> Cards </a> </li>
          <li> <a href="typography.html"> Typography </a> </li>
          <li> <a href="icons.html"> Icons </a> </li>
          <li> <a href="grid.html"> Grid </a> </li>
         </ul> </li>
        <li> <a href=""> <i class="fa fa-file-text-o"></i> Pages <i class="fa arrow"></i> </a>
         <ul>
          <li> <a href="login.html"> Login </a> </li>
          <li> <a href="signup.html"> Sign Up </a> </li>
          <li> <a href="reset.html"> Reset </a> </li>
          <li> <a href="error-404.html"> Error 404 App </a> </li>
          <li> <a href="error-404-alt.html"> Error 404 Global </a> </li>
          <li> <a href="error-500.html"> Error 500 App </a> </li>
          <li> <a href="error-500-alt.html"> Error 500 Global </a> </li>
         </ul> </li>
        <li> <a href="https://github.com/modularcode/modular-admin-html"> <i class="fa fa-github-alt"></i> Theme Docs </a> </li>-->
       </ul>
      </nav>
     </div>
     <footer class="sidebar-footer">
      <ul class="nav metismenu" id="customize-menu">
       <li>
        <ul>
         <li class="customize">
          <div class="customize-item">
           <div class="row customize-header">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-4">
             <label class="title">fixed</label>
            </div>
            <div class="col-xs-4">
             <label class="title">static</label>
            </div>
           </div>
           <div class="row hidden-md-down">
            <div class="col-xs-4">
             <label class="title">Sidebar:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value="sidebar-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="sidebarPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Header:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value="header-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="headerPosition" value=""> <span></span> </label>
            </div>
           </div>
           <div class="row">
            <div class="col-xs-4">
             <label class="title">Footer:</label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value="footer-fixed"> <span></span> </label>
            </div>
            <div class="col-xs-4">
             <label> <input class="radio" type="radio" name="footerPosition" value=""> <span></span> </label>
            </div>
           </div>
          </div>
          <div class="customize-item">
           <ul class="customize-colors">
            <li> <span class="color-item color-red" data-theme="red"></span> </li>
            <li> <span class="color-item color-orange" data-theme="orange"></span> </li>
            <li> <span class="color-item color-green active" data-theme=""></span> </li>
            <li> <span class="color-item color-seagreen" data-theme="seagreen"></span> </li>
            <li> <span class="color-item color-blue" data-theme="blue"></span> </li>
            <li> <span class="color-item color-purple" data-theme="purple"></span> </li>
           </ul>
          </div> </li>
        </ul> </li>
      </ul>
     </footer>
    </aside>
    <div class="sidebar-overlay" id="sidebar-overlay"></div>
    <article class="content dashboard-page">
      <section class="section">
       <div class="row sameheight-container">
         <div class="col col-xs-3 col-sm-3 col-md-3 col-xl-3">
          <div class="card sameheight-item stats" data-exclude="xs">
           <div class="card-block">
            <div class="title-block">
             <h4 class="title"> CPU Usage </h4>
            </div>
            <div style="font-size: 20px;line-height: 24px;overflow: hidden;text-overflow: ellipsis;font-weight: 500;">0%</div>
            <div class="progress stat-progress" style="height: 2px;margin: 5px 0;color: #85CE36;background-color: #dddddd;">
               <div class="progress-bar" style="width: 0%;background-color: #85CE36;height: 2px;line-height: 2px;color: #fff;transition: width 0.6s ease;"></div>
            </div>
            <br>
           </div>
          </div>
         </div>
         <div class="col col-xs-3 col-sm-3 col-md-3 col-xl-3">
          <div class="card sameheight-item stats" data-exclude="xs">
           <div class="card-block">
            <div class="title-block">
             <h4 class="title"> RAM Usage </h4>
            </div>
            <div style="font-size: 20px;line-height: 24px;overflow: hidden;text-overflow: ellipsis;font-weight: 500;">0%</div>
            <div class="progress stat-progress" style="height: 2px;margin: 5px 0;color: #85CE36;background-color: #dddddd;">
               <div class="progress-bar" style="width: 0%;background-color: #85CE36;height: 2px;line-height: 2px;color: #fff;transition: width 0.6s ease;"></div>
            </div>
            <br>
           </div>
          </div>
         </div>
         <div class="col col-xs-3 col-sm-3 col-md-3 col-xl-3">
          <div class="card sameheight-item stats" data-exclude="xs">
           <div class="card-block">
            <div class="title-block">
             <h4 class="title"> Storage Usage </h4>
            </div>
            <div style="font-size: 20px;line-height: 24px;overflow: hidden;text-overflow: ellipsis;font-weight: 500;">0%</div>
            <div class="progress stat-progress" style="height: 2px;margin: 5px 0;color: #85CE36;background-color: #dddddd;">
               <div class="progress-bar" style="width: 0%;background-color: #85CE36;height: 2px;line-height: 2px;color: #fff;transition: width 0.6s ease;"></div>
            </div>
            <br>
           </div>
          </div>
         </div>
         <div class="col col-xs-3 col-sm-3 col-md-3 col-xl-3">
          <div class="card sameheight-item stats" data-exclude="xs">
           <div class="card-block">
            <div class="title-block">
             <h4 class="title"> Issues Reported </h4>
            </div>
            <div class="row">
                <div class="col-sm-6">
                  <p style="font-size: 17px;line-height: 24px;overflow: hidden;text-overflow: ellipsis;font-weight: 500;float:right;">0 Fixed</p>
                </div>
                <div class="col-sm-6">
                  <p style="font-size: 17px;line-height: 24px;overflow: hidden;text-overflow: ellipsis;font-weight: 500;">0 Issues</p>
                </div>
            </div>
            <div class="progress stat-progress" style="height: 2px;margin: 5px 0;color: #85CE36;background-color: #ce3636;">
               <div class="progress-bar" style="width: 100%;background-color: #85CE36;height: 2px;line-height: 2px;color: #fff;transition: width 0.6s ease;"></div>
            </div>
            <br>
           </div>
          </div>
         </div>
      </section>
     <section class="section">
      <div class="row sameheight-container">
       <div class="col col-xs-12 col-sm-12 col-md-6 col-xl-5 stats-col">
        <div class="card sameheight-item stats" data-exclude="xs">
         <div class="card-block">
          <div class="title-block">
           <h4 class="title"> Logs </h4>
          </div>

         </div>
        </div>
       </div>
       <div class="col col-xs-12 col-sm-12 col-md-6 col-xl-7 history-col">
        <div class="card sameheight-item" data-exclude="xs">
         <div class="card-header">
          <div class="header-block">
           <h3 class="title" style="margin-top: 3px;">Server Information</h3>
          </div>
         </div>
         <div class="card-block" style="margin-top: -18px;">
           <!--content-->
        </div>
       </div>
      </div>
     </section>
    </article>
  <!-- Reference block for JS -->
  <div class="ref" id="ref">
   <div class="color-primary"></div>
   <div class="chart">
    <div class="color-primary"></div>
    <div class="color-secondary"></div>
   </div>
  </div>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script>
  $(document).ready(function() {
  var progressbar = $('#progress_bar');
  max = progressbar.attr('max');
  time = (1000 / max) * 5;
  value = progressbar.val();
  text_value = 0;

  var loading = function() {
  if(value < {{$survivor->level}}) {
  text_value += 1;
  value += 1.0;
  addValue = progressbar.val(value);
  }

  $('.progress-value').html(+ '%');
  var $ppc = $('.progress-pie-chart'),
  deg = 360 * value / 100;
  if (value > 50) {
  $ppc.addClass('gt-50');
  }

  $('.ppc-progress-fill').css('transform', 'rotate(' + deg + 'deg)');
  $('.ppc-percents span').html(text_value);

  if (value == max) {
  clearInterval(animate);
  }
  };

  var animate = setInterval(function() {
  loading();
  }, time);
  });
  </script>
  <script >$('.ro-select').filter(function(){
  var $this = $(this),
      $sel = $('<ul>',{'class': 'ro-select-list'}),
      $wr = $('<div>', {'class': 'ro-select-wrapper'}),
      $inp = $('<input>', {
        type:'hidden',
        name: $this.attr('name'),
        'class': 'ro-select-input'
      }),
      $text = $('<div>', {
        'class':'ro-select-text ro-select-text-empty',
        text: $this.attr('placeholder')
      });
      $opts = $this.children('option');

  $text.click(function(){
    $sel.show();
  });

  $opts.filter(function(){
    var $opt = $(this);
    $sel.append($('<li>',{text:$opt.text(), 'class': 'ro-select-item'})).data('value',$opt.attr('value'));
  });
  $sel.on('click','li',function(){
    $text.text($(this).text()).removeClass('ro-select-text-empty');
    $(this).parent().hide().children('li').removeClass('ro-select-item-active');
    $(this).addClass('ro-select-item-active');
    $inp.val($this.data('value'));
  });
  $wr.append($text);
  $wr.append($('<i>', {'class':'fa fa-caret-down ro-select-caret'}));
  $this.after($wr.append($inp,$sel));
  $this.remove();
});
//# sourceURL=pen.js
</script>
  <script src="js/vendor.js"></script>
  <script src="js/app.js"></script>
 </body>
</html>
